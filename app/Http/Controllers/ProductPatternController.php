<?php

namespace App\Http\Controllers;

use App\Models\Product_Group;
use App\Models\Product_Pattern;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductPatternController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:product-pattern-list|product-pattern-create|product-pattern-edit|product-pattern-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:product-pattern-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:product-pattern-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:product-pattern-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        $product_Patterns = Product_Pattern::orderBy('product_pattern_id', 'ASC')->paginate(15);
        return view('product_Patterns.index', compact('product_Patterns'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product_Patterns.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'product_pattern_id' => 'required',
            'product_pattern_code' => 'required',
            'product_pattern_name' => 'required',
        ]);


        $request['created_by']  = Auth::id();
        Product_Pattern::create($request->all());

        return redirect()->route('product_Patterns.index')
            ->with('success', 'Product_Pattern created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product_Pattern  $product_Pattern
     * @return \Illuminate\Http\Response
     */
    public function show(Product_Pattern $product_Pattern)
    {
        return view('product_Patterns.show', compact('product_Pattern'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product_Pattern  $product_Pattern
     * @return \Illuminate\Http\Response
     */
    public function edit(Product_Pattern $product_Pattern)
    {
        return view('product_Patterns.edit', compact('product_Pattern'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product_Pattern  $product_Pattern
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product_Pattern $product_Pattern)
    {
        request()->validate([
            'product_pattern_id' => 'required',
            'product_pattern_code' => 'required',
            'product_pattern_name' => 'required',

        ]);
        $update = [];
        if ($files = $request->file('img')) {
            $destinationPath = 'public/image/'; // upload path
            // $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $org_file_name = $files->getClientOriginalName();
            $extension = $files->getClientOriginalExtension();
            $filename = $org_file_name . $extension;
            $files->move($destinationPath, $filename);
            $update['img'] = "$org_file_name";
        }

        $update['product_pattern_id'] = $request->get('product_pattern_id');
        $update['product_pattern_code'] = $request->get('product_pattern_code');
        $update['product_pattern_name'] = $request->get('product_pattern_name');
        $update['created_by']  = Auth::id();
        $product_Pattern->update($request->all());

        return redirect()->route('product_Patterns.index')
            ->with('success', 'Product_Pattern updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product_Pattern  $product_Pattern
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product_Pattern $product_Pattern)
    {
        $product_Pattern->delete();

        return redirect()->route('product_Patterns.index')
            ->with('success', 'Product_Pattern deleted successfully');
    }
}
