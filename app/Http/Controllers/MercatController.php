<?php

namespace App\Http\Controllers;

use App\Models\Mercat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MercatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:mercat-list|mercat-create|mercat-edit|mercat-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:mercat-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:mercat-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:mercat-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        $mercats = Mercat::orderBy('name', 'ASC')->paginate(15);
        return view('mercats.index', compact('mercats'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mercats.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            

        ]);

      
        $request['created_by']  = Auth::id();
        Mercat::create($request->all());

        return redirect()->route('mercats.index')
            ->with('success', 'Mercat created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Mercat  $mercat
     * @return \Illuminate\Http\Response
     */
    public function show(Mercat $mercat)
    {
        return view('mercats.show', compact('mercat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Mercat  $mercat
     * @return \Illuminate\Http\Response
     */
    public function edit(Mercat $mercat)
    {
        return view('mercats.edit', compact('mercat'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Mercat  $mercat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mercat $mercat)
    {
        request()->validate([
            'name' => 'required',
        ]);
        $update = [];
        if ($files = $request->file('img')) {
            $destinationPath = 'public/image/'; // upload path
            // $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $org_file_name = $files->getClientOriginalName();
            $extension = $files->getClientOriginalExtension();
            $filename = $org_file_name . $extension;
            $files->move($destinationPath, $filename);
            $update['img'] = "$org_file_name";
        }


        $update['name'] = $request->get('name');
        $update['created_by']  = Auth::id();

        $mercat->update($request->all());

        return redirect()->route('mercats.index')
            ->with('success', 'Mercat updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Mercat  $mercat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mercat $mercat)
    {
        $mercat->delete();

        return redirect()->route('mercats.index')
            ->with('success', 'Mercat deleted successfully');
    }
}
