<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Depts;
use App\Models\Mercat;
use App\Models\User;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;
use Symfony\Contracts\Service\Attribute\Required;

class EmployeeController extends Controller
{


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    public function __construct()
    {
        $this->middleware('role:SD', ['only' => ['register']]);
    }
    protected function register()
    {
        // $roles = Role::pluck('name', 'name')->all();
        $roles =  Role::where('id', '>', 1)->get();
        $branches = Branch::where('id','=',7)->get();
        $mercats = Mercat::all();
        $depts = Depts::all();
        return view('employee.register', compact('branches', 'depts', 'mercats', 'roles'));
    }

    protected function login()
    {
        return view('employee.login');
    }
    protected function user_reset_pwd()
    {
        return view('employee.reset_pwd');
    }
    protected function reset_pwd(Request $request)
    {
        $this->validate($request, [
            'ph_no' => 'nullable',
            'employee_id' => 'required',
            'password' => 'required|same:confirm-password|min:6',
        ]);

        $input = $request->all();
        if (!empty($input['ph_no'])) {
            $phno = $input['ph_no'];
            $result = DB::table('users')->where('ph_no',  $phno)->first();
            if (empty($result)) {
                // Arr::except($input,array('ph_no'));    
                return redirect('user_reset_pwd')->withErrors(['ph_no' => 'Phone No is not register in this system']);
            }
        } else {
            unset($input['ph_no']);
        }
        if (!empty($input['employee_id'])) {
            $employee_id = $input['employee_id'];
            $result = DB::table('users')->where('employee_id',  $employee_id)->first();
            if (empty($result)) {
                return redirect('user_reset_pwd')->withErrors(['employee_id' => 'Employee Code is not register in this system']);
            }
        } else {
            unset($input['employee_id']);
        }
        $input['password'] = Hash::make($input['password']);
        $user = User::find($result->id);
        $user->update($input);

        return view('employee.login');
    }
    protected function check_login(Request $request)
    {
        $this->validate($request, [
            // |digit:10   |regex:/^([0-9\s\-\+\(\)]*)$/|min:9|max:11
            'login_value' => 'required',
            'password' => 'required|min:6',
        ]);

        $input = $request->all();
        // if else  @ email
        $login_value =  $input['login_value'];
        $email = '';
        $pattern_1 = '/^([0-9\s\-\+\(\)]*)$/';
        $case_1 = preg_match($pattern_1, $login_value);
        $pattern_2 = filter_var($login_value, FILTER_VALIDATE_EMAIL);
        $case_2 = $pattern_2 ? 1 : 0;
        $pattern_3 = '/^\d{3}(-\d{6})?$/';
        $case_3 = preg_match($pattern_3, $login_value);
        if ($case_3 === 1) {
            $user = User::where('employee_id', $login_value)->first();
        } else if ($case_2 === 1) {
            $user = User::where('email', $login_value)->first();
        } else if ($case_1 === 1) {
            $user = User::where('ph_no', $login_value)->first();
        } else {
            return redirect()->to('user_login')->withInput()->withErrors(['login_value' => 'Something was wrong. Please Try Again...!!']);
        }
        if (isset($user)) {
            // Set Auth Details
            if (Hash::check($input['password'], $user->password)) {
                Auth::login($user);
                // Redirect home page
                return redirect()->route('home');
            } else {
                return redirect()->route('user_login')->withInput()->withErrors(['password' => 'Password is not correct']);
            }
        } else {
            return redirect()->to('user_login')->withInput()->withErrors(['login_value' => 'Something was wrong...!!']);
        }
    }
    protected function otp_input()
    {
        return view('employee.otp_input');
    }

    /////DropdownAjax//////
    public function index()
    {
        $branches = Branch::get();
        return view('employee.register', compact('branches'));
    }
    public function getBranch($branch_id)
    {
        $data = Depts::where('branch_id', $branch_id)->get();
        // dd($data);
        Log::info($data);
        return response()->json(['data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'  => 'required',
            'email' => 'nullable|email|unique:users,email',
            'ph_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:9|max:11|unique:users,ph_no',
            'password' => 'required|same:confirm-password|min:6',
            'employee_id' => 'nullable|regex:/^\d{3}(-\d{6})?$/',
        ]);
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['otp'] = $this->create_OTP();
        $user = User::create($input);
        $user->assignRole($input['role']);
        // comment out the below line to sent otp 
        // $this->send_OTP($input['ph_no'], $input['otp']);
        $ph_no = $input['ph_no'];
        // comment out the below line to check otp 
        //$otp = Hash::make($input['otp']); 
        $otp = $input['otp'];
        $err = NUll;
        return view('employee.otp_input', compact('ph_no', 'otp', 'err'));
    }

    /*
    ** Random 6 digits OTP Genearate function added by Hein @ Jan 7 2021
    */
    protected function create_OTP()
    {
        $otp = mt_rand(000000, 999999);
        return $otp;
    }

    /*
    ** Sent OTP via SMSPoh API function added  by Hein @ Jan 7 2021
    */
    protected function send_OTP($ph_no, $otp)
    {
        // SMSPoh Authorization Token
        $token =  config('app.smspoh_token');
        // Prepare data for POST request
        $data = [
            "to"        =>      $ph_no,
            "message"   =>      $otp . " is OTP code for PRO1 GLOBAL ERP CODE",
            "sender"    =>      "Pro1 Global"
        ];
        $url = config('app.smspoh_url');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer ' . $token,
            'Content-Type: application/json'
        ]);
        curl_exec($ch);
    }

    protected function otp_check(Request $request)
    {
        $input = $request->all();
        /*  cmd out the below two line to otp check function  */
        User::where('ph_no', $input['ph_no'])->update(array('otp' => NULL, 'otp_verified_at' => now()));
        return redirect()->route('home');

        /* otp check function code cmd out 
         if (Hash::check($input['confirm-otp'], $input['otp'])) {
            // update otp null and otp verified at   
            User::where('ph_no', $input['ph_no'])->update(array('otp' => NULL, 'otp_verified_at' => now()));
            return redirect()->route('user_login');
        } else {

            $ph_no = $input['ph_no'];
            $otp = $input['otp'];
            $err = 'OTP not Correct';
            return view('employee.otp_input', compact('ph_no', 'otp', 'err'));
        }
        */
    }
    public function change_pwd()
    {
        $id = Auth::id();
        $user = User::find($id);
        $roles = Role::pluck('name', 'name')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();
        return view('users.change_pwd', compact('user', 'roles', 'userRole'));
    }


    public function update_pwd(Request $request, $id)
    {
        $this->validate($request, [
            'password' => 'same:confirm-password'

        ]);
        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }
        $user = User::find($id);
        $user->update($input);
        return redirect()->route('users.index')
            ->with('success', 'Password updated successfully');
    }
}
