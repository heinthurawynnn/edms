<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use ArrayObject;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables as FacadesDataTables;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:branch-list|branch-create|branch-edit|branch-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:branch-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:branch-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:branch-delete', ['only' => ['destroy']]);
    }
    public function index(Request $request)
    {
        $branches = Branch::latest()->get();
        // $branches = Branch::select('branch_id')->where('branch_active', '=', "true")->get();
        // return view('branches.index', );
        return view('branches.index', compact('branches'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('branches.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'address' => 'required'

        ]);

        $request['created_by']  = Auth::id();
        Branch::create($request->all());

        return redirect()->route('branches.index')
            ->with('success', 'Brand created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function show(Branch $branch)
    {
        return view('branches.show', compact('branch'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function edit(Branch $branch)
    {
        return view('branches.edit', compact('branch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Branch $branch)
    {
        request()->validate([
            'name' => 'required',
        ]);
        // $update = [];
        // if ($files = $request->file('img')) {
        //     $destinationPath = 'public/image/'; // upload path
        //     // $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
        //     $org_file_name = $files->getClientOriginalName();
        //     $extension = $files->getClientOriginalExtension();
        //     $filename = $org_file_name . $extension;
        //     $files->move($destinationPath, $filename);
        //     $update['img'] = "$org_file_name";
        // }
        // $update['name'] = $request->get('name');
        // $update['short_desc'] = $request->get('short_desc');
        // $update['link'] = $request->get('link');
        // $update['created_by']  = Auth::id();
        // $branch->update($request->all());
        // return redirect()->route('branches.index')
        //     ->with('success', 'Brand updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Branch $branch)
    {
        $branch->delete();

        return redirect()->route('branches.index')
            ->with('success', 'Brand deleted successfully');
    }
}
