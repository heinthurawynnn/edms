<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:brand-list|brand-create|brand-edit|brand-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:brand-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:brand-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:brand-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        $brands = Brand::latest()->paginate(50);
        return view('brands.index', compact('brands'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'product_brand_code' => 'required',
            'product_brand_name' => 'required',
            

            // 'name' => 'required',
            // 'short_desc' => 'required',
            // 'img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'

        ]);

        // $imageName = $request->img->getClientOriginalName();
        // $request->img->move(public_path('assets/product_upload_img'), $imageName);
        // $request['img']  = $imageName;
        // $request['created_by']  = Auth::id();
        Brand::create($request->all());

        return redirect()->route('brands.index')
            ->with('success', 'Brand created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        return view('brands.show', compact('brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        return view('brands.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        request()->validate([
            'product_brand_code' => 'required',
            'product_brand_name' => 'required',
            
            // 'name' => 'required',
            // 'short_desc' => 'required',
        ]);
        // $update = [];
        // if ($files = $request->file('img')) {
        //     $destinationPath = 'public/image/'; // upload path
        //     // $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
        //     $org_file_name = $files->getClientOriginalName();
        //     $extension = $files->getClientOriginalExtension();
        //     $filename = $org_file_name . $extension;
        //     $files->move($destinationPath, $filename);
        //     $update['img'] = "$org_file_name";
        // }


        // $update['name'] = $request->get('name');
        // $update['short_desc'] = $request->get('short_desc');
        // $update['link'] = $request->get('link');
        // $update['created_by']  = Auth::id();

        $brand->update($request->all());

        return redirect()->route('brands.index')
            ->with('success', 'Brand updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        $brand->delete();

        return redirect()->route('brands.index')
            ->with('success', 'Brand deleted successfully');
    }
}
