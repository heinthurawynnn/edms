<?php

namespace App\Exports;

use App\Models\Document;
use App\Models\ProductCode;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithProperties;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Reader\Xls\Style\Border as StyleBorder;
use PhpOffice\PhpSpreadsheet\Style\Border;

class DocumentExport implements FromView, WithEvents, WithTitle, WithProperties
{
    protected $doc_id;
    use RegistersEventListeners;

    function __construct($doc_id)
    {
        $this->doc_id = $doc_id;
    }
    public function view(): View
    {
        $document = Document::find($this->doc_id);
        $products = ProductCode::where('doc_no', $this->doc_id)->orderByDesc('id')->paginate(20);
        return view('document_excel_view', compact('products', 'document'))->with('i', (request()->input('page', 1) - 1) * 20);
    }
    public function title(): string
    {
        $document = Document::find($this->doc_id);
        return $document->document_no;
    }
    public static function afterSheet(AfterSheet $event)
    {


        // Create Style Arrays
        $default_font_style = [
            'font' => ['name' => 'Arial', 'size' => 10]
        ];
        // Get Worksheet
        $active_sheet = $event->sheet->getDelegate();


        // Apply Style Arrays
        $active_sheet->getParent()->getDefaultStyle()->applyFromArray($default_font_style);
        // wrap text true   

        $active_sheet->getStyle('A1:Z999')->getAlignment()->setWrapText(true);
        // $active_sheet->setb('thin');
        $active_sheet->getStyle('A1:K3')->applyFromArray(array(
            'font' => array(
                'name'      =>  'Arial',
                'size'      =>  12,
                'bold'      =>  true
            )
        ));
        $active_sheet->getStyle('A3:K17')->applyFromArray(array(
            'borders' => [
                'allBorders' => [
                    'borderSytle'=> \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => [ 'argb' => '000000'],
                    ],
            ]
        ));
        // $active_sheet->getStyle('A13')->getFont()->setStrikethrough(true);
        ////creator name////
        // $active_sheet->setcreator('name');
        

    }
    public function properties(): array
    {
        return [
            'creator' => 'PRO1 New Code Request System',
            'company' => 'PRO 1 GLOBAL'
        ];
    }
}
