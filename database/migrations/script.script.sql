@for($i=0, $i
                < 4; ++$i;) {{$i}} @endfor
insert into master_data.master_product(
        lotserialexpireflag, --(0)
        product_pack_flag,   --(G/P/S)
        vartype,
        commission_flag,
        product_type_flag,
        inactive,    --(A/I)
        xposflag,
        xpostime,
        editflag,
        discflag,
        priceflag,
        cardcredit_charge,
        product_id,
        product_code,
        product_name1,
        product_name2,
        product_name_eng1,
        product_bill_name,

        main_product_unit_id,
        product_category_id,
        product_group_id,
        product_pattern_id,
        product_design_id,
        product_brand_id,
        product_type_id, 
        product_name_eng2
) values(
        '0',
        'G',
        '1',
        'N',
        'G',
        'A',
        'Y',
        now(),
        'N',
        'Y',
        'N',
        true,
         ,   
        '8010303030309',
        'Min Hex Key Spanner Size-14mm,
        22mm,
        27mm A00303',
        'Min Hex Key Spanner Size-14mm,
        22mm,
        27mm A00303',
        'Min Hex Key Spanner Size-14mm,
        22mm,
        27mm A00303',
        'Min Hex Key Spanner Size-14mm,
        22mm,
        27mm A00303',
        1,
        6,
        48,
        219,
        220,
        1935,
        '3',
        'added on 2019/03/23'
    );
    
insert into master_data.master_product_multiunit(
        product_id,
        product_code,
        product_unit_id, 
        product_unit_rate,--(1)
        storage_unit_flag,
        stock_unit_flag
    )
values(
        ,
        '8010303030309',
        1,
        1,
        true,
        true
    );
insert into master_data.master_product_multivendor(
        product_id,
        list_no, --(0 to 14)
        product_code,
        barcode_code,
        vendor_id,
        product_unit_id
    )
values(
        ,
        1,
        '8010303030309',
        '8010303030309',
        110,
        1
    );
insert into master_data.master_product_multiprice(
        product_id,
        product_code,
        barcode_code,
        product_unit_id,
        branch_id,
        listno,
        product_price1, --(0)
        product_price2, --(0)
        product_price3,
        product_price4,
        product_price5,
        product_price6,
        product_price7,
        product_price8,
        product_price9,
        product_price10
    )
values(
        ,
        '8010060206023',
        '8010060206023',
        1,
        1,
        1,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    );








select max(id)
from vendor
insert into vendor (vendor_code)
values ('0-0000')
select *
from vendor --where vendor_code = '0-0000' 
order by id desc delete
insert into vendor(vendor_code, vendor_name)
select vendor_code,
    vendor_name
from dblink(
        'dbname=master_product_erp host = 192.168.2.248 port=5432 user=postgres password=p@ssw0rd',
        '
select vendor_code,vendor_name
			   from category.vendor'
    ) as temp(
        vendor_code character varying(25),
        vendor_name character varying(255)
    );
insert into brand(product_brand_code, product_brand_name)
select product_brand_code,
    product_brand_name
from dblink(
        'dbname=master_product_erp host = 192.168.2.248 port=5432 user=postgres password=p@ssw0rd',
        '
select product_brand_code,product_brand_name
			   from category.brand'
    ) as temp(
        product_brand_code character varying(255),
        product_brand_name character varying(255)
    );
insert into unit(product_unit_code, product_unit_name)
select product_unit_code,
    product_unit_name
from dblink(
        'dbname=master_product_erp host = 192.168.2.248 port=5432 user=postgres password=p@ssw0rd',
        '
select product_unit_code,product_unit_name
			   from category.unit'
    ) as temp(
        product_unit_code character varying(255),
        product_unit_name character varying(255)
    );
insert into category(
        product_category_id,
        product_group_id,
        product_pattern_id,
        product_design_id,
        main_category,
        product_category_code,
        product_category_name,
        product_group_code,
        product_group_name,
        product_pattern_code,
        product_pattern_name,
        product_design_code,
        product_design_name,
        type
    )
select *
from dblink(
        'dbname=master_product_erp host = 192.168.2.248 port=5432 user=postgres password=p@ssw0rd',
        '
select product_category_id,product_group_id,
       product_pattern_id,product_design_id,
	   main_category,
	   product_category_code,product_category_name,
	   product_group_code,product_group_name,
	   product_pattern_code,product_pattern_name,
	   product_design_code,product_design_name,type
			   from category.category'
    ) as temp(
        product_category_id integer,
        product_group_id integer,
        product_pattern_id integer,
        product_design_id integer,
        main_category character varying(255),
        product_category_code character varying(10),
        product_category_name character varying(255),
        product_group_code character varying(10),
        product_group_name character varying(255),
        product_pattern_code character varying(10),
        product_pattern_name character varying(255),
        product_design_code character varying(10),
        product_design_name character varying(255),
        type character varying(10)
    );
insert into master_data.master_product (
        lotserialexpireflag,
        product_pack_flag,
        vartype,
        commission_flag,
        product_type_flag,
        inactive,
        xposflag,
        xpostime,
        editflag,
        discflag,
        priceflag,
        cardcredit_charge,
        product_id,
        product_code,
        product_name1,
        product_name2,
        product_name_eng1,
        product_bill_name,
        main_product_unit_id,
        product_category_id,
        product_group_id,
        product_pattern_id,
        product_design_id,
        product_brand_id,
        product_type_id,
        product_name_eng2
    )
values (
        '0',
        'G',
        '1',
        'N',
        'G',
        'A',
        'Y',
        now(),
        'N',
        'Y',
        'N',
        true,
,
        '8010303030309',
        'Min Hex Key Spanner Size-14mm,
    22mm,
    27mm A00303',
        'Min Hex Key Spanner Size-14mm,
    22mm,
    27mm A00303',
        'Min Hex Key Spanner Size-14mm,
    22mm,
    27mm A00303',
        'Min Hex Key Spanner Size-14mm,
    22mm,
    27mm A00303',
        1,
        6,
        48,
        219,
        220,
        1935,
        '3',
        'added on 2019/03/23'
    );
/ / / Product Group Code Start / / /
insert into product_groups (
        product_group_id,
        product_category_id,
        product_group_code,
        product_group_name
    )
select product_group_id,
    product_category_id,
    product_group_code,
    product_group_name
from dblink(
        'dbname=master_product_erp host = 192.168.2.248 port=5432 user=postgres password=p@ssw0rd',
        'select   distinct on (product_group_id)product_group_id, product_category_id,product_group_code, product_group_name
				   from category.category '
    ) as temp(
        product_group_id integer,
        product_category_id integer,
        product_group_code character varying(10),
        product_group_name character varying(255)
    );
/ / / Product Group Code
End / / / / / / Product Pattern Code Start / / /
insert into product_patterns (
        product_pattern_id,
        product_group_id,
        product_pattern_code,
        product_pattern_name
    )
select product_pattern_id,
    product_group_id,
    product_pattern_code,
    product_pattern_name
from dblink(
        'dbname=master_product_erp host = 192.168.2.248 port=5432 user=postgres password=p@ssw0rd',
        'select   distinct on (product_pattern_id)product_pattern_id, product_group_id,product_pattern_code, product_pattern_name
				   from category.category '
    ) as temp(
        product_pattern_id integer,
        product_group_id integer,
        product_pattern_code character varying(10),
        product_pattern_name character varying(255)
    );
/ / / Product Pattern Code
End / / / / / / Product Design Code Start / / /
insert into product_designs (
        product_design_id,
        product_pattern_id,
        product_design_code,
        product_design_name
    )
select product_design_id,
    product_pattern_id,
    product_design_code,
    product_design_name
from dblink(
        'dbname=master_product_erp host = 192.168.2.248 port=5432 user=postgres password=p@ssw0rd',
        'select   distinct on (product_design_id)product_design_id, product_pattern_id,product_design_code, product_design_name
				   from category.category '
    ) as temp(
        product_design_id integer,
        product_pattern_id integer,
        product_design_code character varying(10),
        product_design_name character varying(255)
    );
/ / / Product Design Code
End / / /