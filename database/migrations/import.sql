backup
/usr/bin/pg_dump --file "29_apr.backup" --host "192.168.2.244" --port "5432" --username "postgres" --verbose --format=c --blobs "pro1_erp_code"


scp 192.168.2.244:/var/www/html/erp_code/29_apr.backup .

-- Category --
insert into product_categories (
        id,
        product_category_id,
        product_category_code,
        product_category_name
    )
select 
    product_category_id,
    product_category_code,
    product_category_name
from dblink(
        'dbname=pro1 host = 192.168.2.241 port=5432 user=postgres password=p@ssw0rd',
        'select   distinct on (product_category_id)product_category_id,product_category_code, product_category_name
				   from master_data.master_product_category '
    ) as temp(
        product_category_id integer,
        product_category_code character varying(10),
        product_category_name character varying(255)
    );

-- Group --
insert into product_groups (
        id,
        product_group_id,
        product_category_id,
        product_group_code,
        product_group_name
    )
select 
    product_group_id,
    product_group_id,
    product_category_id,
    product_group_code,
    product_group_name
from dblink(
        'dbname=pro1 host = 192.168.2.241 port=5432 user=postgres password=p@ssw0rd',
        'select   distinct on (product_group_id)product_group_id, product_category_id,product_group_code, product_group_name
				   from master_data.master_product_group '
    ) as temp(
        product_group_id integer,
        product_category_id integer,
        product_group_code character varying(10),
        product_group_name character varying(255)
    );

-- Design --
insert into product_designs (
        id,
        product_design_id,
        product_pattern_id,
        product_design_code,
        product_design_name
    )
select product_design_id,
    product_pattern_id,
    product_design_code,
    product_design_name
from dblink(
        'dbname=pro1 host = 192.168.2.241 port=5432 user=postgres password=p@ssw0rd',
        'select   distinct on (product_design_id)product_design_id, product_pattern_id,product_design_code, product_design_name
				   from master_data.master_product_design'
    ) as temp(
        product_design_id integer,
        product_pattern_id integer,
        product_design_code character varying(10),
        product_design_name character varying(255)
    );

-- Pattern --
insert into product_patterns (
        id,
        product_pattern_id,
        product_group_id,
        product_pattern_code,
        product_pattern_name
    )
select product_pattern_id,
    product_group_id,
    product_pattern_code,
    product_pattern_name
from dblink(
        'dbname=pro1 host = 192.168.2.241 port=5432 user=postgres password=p@ssw0rd',
        'select   distinct on (product_pattern_id)product_pattern_id, product_group_id,product_pattern_code, product_pattern_name
				   from master_data.master_product_pattern '
    ) as temp(
        product_pattern_id integer,
        product_group_id integer,
        product_pattern_code character varying(10),
        product_pattern_name character varying(255)
    );
-- design --
insert into product_designs (
        id,
        product_design_id,
        product_pattern_id,
        product_design_code,
        product_design_name
    )
select product_design_id,
    product_pattern_id,
    product_design_code,
    product_design_name
from dblink(
        'dbname=pro1 host = 192.168.2.241 port=5432 user=postgres password=p@ssw0rd',
        'select   distinct on (product_design_id)product_design_id, product_pattern_id,product_design_code, product_design_name
				   from master_data.master_product_design'
    ) as temp(
        product_design_id integer,
        product_pattern_id integer,
        product_design_code character varying(10),
        product_design_name character varying(255)
    );

-- vendor --
insert into vendor (
		id,
        vendor_code,
        vendor_name
    )
select vendor_id,
    vendor_code,
    vendor_name
from dblink(
        'dbname=pro1 host = 192.168.2.241 port=5432 user=postgres password=p@ssw0rd',
        'select   distinct on (vendor_id)vendor_id,vendor_code,vendor_name
				  from configure.setap_vendor '
    ) as temp(
        vendor_id integer,
        vendor_code character varying(25),
        vendor_name character varying(255)
    );

-- brand --
insert into brand (
		id,
        product_brand_code,
        product_brand_name
    )
select product_brand_id,
    product_brand_code,
    product_brand_name
from dblink(
        'dbname=pro1 host = 192.168.2.241 port=5432 user=postgres password=p@ssw0rd',
        'select   distinct on (product_brand_id)product_brand_id,product_brand_code,product_brand_name
				  from master_data.master_product_brand '
    ) as temp(
        product_brand_id integer,
        product_brand_code character varying(50),
        product_brand_name character varying(255)
    );

    ---Branch---

    insert into branches (
        id,
        branch_code,
        short_name,
        name,
        address,
	    phone_no,
	    branch_active
    )
select  branch_id,
        branch_code,
        branch_short_name,
        branch_name,
        branch_address1,
	    tel,
		branch_active
from dblink(
        'dbname=pro1 host = 192.168.2.242 port=5432 user=postgres password=p@ssw0rd',
        'select   distinct on (branch_id)branch_id, branch_code,branch_short_name,branch_name,branch_address1
		,tel,branch_active from master_data.master_branch'
    ) as temp(
        branch_id integer,
        branch_code  character varying(10),
        branch_short_name character varying(10),
        branch_name character varying(255),
        branch_address1 character varying(255),
		tel character varying(255),
		branch_active boolean
		
    );
	