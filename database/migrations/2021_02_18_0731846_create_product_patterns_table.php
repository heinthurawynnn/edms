<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPatternsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_patterns', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_pattern_id')->nullable();
            $table->bigInteger('product_group_id')->nullable();
            $table->string('product_pattern_code');
            $table->string('product_pattern_name');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_patterns');
    }
}
