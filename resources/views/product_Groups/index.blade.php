@extends('adminlte::page')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Product Group</h2>
        </div>
        <div class="pull-right mb-3">
            @can('product-group-create')
            <a class="btn btn-success" href="{{ route('product_Groups.create') }}"> Add New Group</a>
            @endcan
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif


<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Product Group Id</th>
        <th>Product Group Code</th>
        <th>Product Group Name</th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($product_Groups as $product_Group)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $product_Group->product_group_id }}</td>
        <td>{{ $product_Group->product_group_code }}</td>
        <td>{{ $product_Group->product_group_name }}</td>
      
        <td>
            <form action="{{ route('product_Groups.destroy',$product_Group->id) }}" method="POST">
                <a class="btn btn-info" href="{{ route('product_Groups.show',$product_Group->id) }}">Show</a>
                @can('product-group-edit')
                <a class="btn btn-primary" href="{{ route('product_Groups.edit',$product_Group->id) }}">Edit</a>
                @endcan


                @csrf
                @method('DELETE')
                @can('product-group-delete')
                <button type="submit" class="btn btn-danger">Delete</button>
                @endcan
            </form>
        </td>
    </tr>
    @endforeach
</table>


{!! $product_Groups->links() !!}


@endsection