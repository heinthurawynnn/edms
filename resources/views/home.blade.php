@extends('layouts.app')

@section('content')

<div>

    <div class="bg-light p-5 home-center-text">
        <h1 class="text-bold text-center "><span class="text-primary font-weight-bold">PRO</span><span class="text-danger font-weight-bold">1</span> <span class="font-weight-bold">Global</span> </h1>
        <h2 class="text-bold text-center bg-dark   m-3 text-info"> ERP CODE REQUEST SYSTEM</h2>
    </div>
</div>
<div class="bg-light mt-5 p-3">
    <h3 class="text-bold text-center text-uppercase p-2 bg-dark text-warning"> User Documentation </h2>
    <object data="document-v2.pdf" type="application/pdf" width="100%" style="min-height: 50vh" class="text-center">
        <p>Alternative text - include a link <a href="document.pdf">to the PDF!</a></p>
    </object>
</div>

@endsection