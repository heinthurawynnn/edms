<!DOCTYPE html>
@extends('layouts.app')

@section('content')


@if ($message = Session::get('success'))
<div class="bg-info p-3">
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
</div>
@endif
<!-- SD View -->
@role('SD')
@if($errors->any())
<div class="bg-info p-3">

    @foreach($errors->all() as $key => $error)
    <div class="bg-light p-3 mb-3 card {{ $key == 0 ? 'd-none' : ''}}">

        @php
        $script_line_arr = explode("doc_id",$error);
        if(!empty($script_line_arr[1])){
        $doc_id = $script_line_arr[1];
        }
        @endphp

        @foreach($script_line_arr as $script_line)
        <div class="form-group">
            <div class="input-group input-group-copy">
                <span class="input-group-btn">
                    <button class="btn btn-success">Copy <i class="fas fa-copy"></i></button>
                </span>
                <input id="" type="text" readonly class="form-control" value="{{ $script_line }}" />
            </div>
        </div>
        @endforeach
    </div>
    @endforeach
    @if($doc_id != 'exported')
    <form action="{{ route('doc_export',$doc_id) }}" method="POST" class="text-right">
        @csrf
        @method('POST')
        <button type="submit" class="btn btn-warning">Done <i class="fas fa-database"></i></button>
    </form>
    @endif
</div>
@endif

<div class="bg-light p-3">

    @if(count($approved) >= 1 )
    <h2 class="text-info text-center p-2 m-2"> Approved Document</h2>
    <table class="table table-bordered">
        <tr>
            <th>Document No</th>
            <th>Items</th>
            <th>Prepared By</th>
            <th>Prepared At</th>
            <th>Approved By</th>
            <th>Approved At</th>
            <th>Exported By</th>
            <th>Exported At</th>
            @can('sd-team-save')
            <th>Action</th>
            @endcan
        </tr>
        @foreach ($approved as $document)
        <tr>
            <td> <a class="" href="{{ route('approve_view',$document->id) }}">{{ $document->document_no}}</a></td>
            @php
            $codes = explode(",", $document->pcode_id);
            $codes_count = count($codes);
            @endphp
            <td>{{ $codes_count }}</td>
            <td>{{ $document->prepared->name }}</td>
            <td>{{ $document->prepared_at }}</td>
            <td>{{ $document->approved_by ? $document->approved->name : "" }}</td>
            <td>{{ $document->approved_at }}</td>
            <td>{{ $document->exported_by ? $document->exported->name : ""}}</td>
            <td>{{ $document->exported_at}}</td>
            @can('sd-team-save')
            <td>
                <form action="{{ route('doc_export',$document->id) }}" method="POST">
                    @csrf
                    @method('POST')
                    <a class="btn btn-info open pr-3" href="{{route('export_script',$document->id) }}">Export <i class="fas fa-file-export"></i></a>
                    <button type="submit" class="btn btn-success">Done <i class="fas fa-database"></i></button>
                </form>
            </td>
            @endcan
        </tr>
        @endforeach
    </table>
    @else
    <p class="text-center"> There was no approved document </p>
    @endif
    @if(count($done_docs) >= 1 )
    <h2 class="text-info text-center p-2 m-2"> Finished Document</h2>
    <table class="table table-bordered">
        <tr>
            <th>Document No</th>
            <th>Items</th>
            <th>Prepared By</th>
            <th>Prepared At</th>
            <th>Approved By</th>
            <th>Approved At</th>
            <th>Exported By</th>
            <th>Exported At</th>
            @can('sd-team-save')
            <th>Action</th>
            @endcan
        </tr>
        @foreach ($done_docs as $document)
        <tr>

            <td> <a class="" href="{{ route('approve_view',$document->id) }}">{{ $document->document_no}}</a></td>
            @php
            $codes = explode(",", $document->pcode_id);
            $codes_count = count($codes);
            @endphp
            <td>{{ $codes_count }}</td>
            <td>{{ $document->prepared->name }}</td>
            <td>{{ $document->prepared_at }}</td>
            <td>{{ $document->approved_by ? $document->approved->name : "" }}</td>
            <td>{{ $document->approved_at }}</td>
            <td>{{ $document->exported_by ? $document->exported->name : ""}}</td>
            <td>{{ $document->exported_at}}</td>
            @can('sd-team-save')
            <td>
                <a class="btn btn-info open" href="{{ route('export_script',$document->id) }}">Export <i class="fas fa-file-export"></i></a>
            </td>
            @endcan
        </tr>
        @endforeach
    </table>

    @endif

</div>
@endrole

<!-- Datatable for Manager -->
@role('Manager')
<div class="p-3">

    <div class="row">
        <div class="col-md-12  text-center py-3 my-2 bg-light">
            <h1 class="text-uppercase align-center m-auto">Document List</h1>
        </div>
    </div>
    <div class="bg-light mt-5 p-3">


        <table class="table table-bordered data-table" id="data_table">
            <thead>
                <tr>
                    <th>Status</th>
                    <th>Document No</th>
                    <th>Category</th>
                    <th>Item</th>
                    <th>Prepared By</th>
                    <th>Prepared At</th>
                    <th>Checked By</th>
                    <th>Checked At</th>
                    <th>Approved By</th>
                    <th>Approved At</th>
                    <th>Exported By</th>
                    <th>Exported At</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    @endrole


</div>
@endsection
@section('js')
<script>
    $(document).ready(function() {
        var inputCopyGroups = document.querySelectorAll('.input-group-copy');
        for (var i = 0; i < inputCopyGroups.length; i++) {
            var _this = inputCopyGroups[i];
            var btn = _this.getElementsByClassName('btn')[0];
            var input = _this.getElementsByClassName('form-control')[0];
            input.addEventListener('click', function(e) {
                this.select();
            });
            btn.addEventListener('click', function(e) {
                var input = this.parentNode.parentNode.getElementsByClassName('form-control')[0];
                input.select();
                try {
                    var success = document.execCommand('copy');
                    console.log('Copying ' + (success ? 'Succeeded' : 'Failed'));
                } catch (err) {
                    console.log('Copying failed');
                }
            });
        }
        /////Datatable////
        $(function() {
            var table = $('#data_table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "{{ route('document_list_for_manager') }}",
                columns: [{
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'document_no',
                        name: 'document_no'
                    },
                    {
                        data: 'category_name',
                        name: 'category_name'
                    },
                    {
                        data: 'pcode_id',
                        name: 'pcode_id'
                    },
                    {
                        data: 'prepared_by',
                        name: 'prepared_by'
                    },
                    {
                        data: 'prepared_at',
                        name: 'prepared_at'
                    },
                    {
                        data: 'checked_by',
                        name: 'checked_by'
                    },
                    {
                        data: 'checked_at',
                        name: 'checked_at'
                    },
                    {
                        data: 'approved_by',
                        name: 'approved_by'
                    },
                    {
                        data: 'approved_at',
                        name: 'approved_at'
                    },
                    {
                        data: 'exported_by',
                        name: 'exported_by'
                    },
                    {
                        data: 'exported_at',
                        name: 'exported_at'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });
        });
    });
</script>

@endsection