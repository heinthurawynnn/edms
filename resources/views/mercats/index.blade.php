@extends('adminlte::page')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Mercategory</h2>
        </div>
        <div class="pull-right mb-3">
            @can('mercat-create')
            <a class="btn btn-success" href="{{ route('mercats.create') }}"> Add New Category</a>
            @endcan
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif


<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Name</th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($mercats as $mercat)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $mercat->name }}</td>
      
        <td>
            <form action="{{ route('mercats.destroy',$mercat->id) }}" method="POST">
                <a class="btn btn-info" href="{{ route('mercats.show',$mercat->id) }}">Show</a>
                @can('mercat-edit')
                <a class="btn btn-primary" href="{{ route('mercats.edit',$mercat->id) }}">Edit</a>
                @endcan


                @csrf
                @method('DELETE')
                @can('mercat-delete')
                <button type="submit" class="btn btn-danger">Delete</button>
                @endcan
            </form>
        </td>
    </tr>
    @endforeach
</table>


{!! $mercats->links() !!}


@endsection