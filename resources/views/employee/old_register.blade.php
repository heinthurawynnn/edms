@extends('adminlte::auth.auth-page', ['auth_type' => 'register'])

@php( $login_url = View::getSection('login_url') ?? config('adminlte.login_url', 'login') )
@php( $register_url = View::getSection('register_url') ?? config('adminlte.register_url', 'register') )

@if (config('adminlte.use_route_url', false))
@php( $login_url = $login_url ? route($login_url) : '' )
@php( $register_url = $register_url ? route($register_url) : '' )
@else
@php( $login_url = $login_url ? url($login_url) : '' )
@php( $register_url = $register_url ? url($register_url) : '' )
@endif

@section('auth_header', __('adminlte::adminlte.register'))

@section('auth_body')

<form action="{{ route('employee.store') }}" method="POST">
    @csrf
    {{ csrf_field() }}
    {{-- Name field --}}
    <div class="input-group mb-3">
        <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" value="{{ old('name') }}" placeholder="Employee Name" autofocus>
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="text-danger" title="Required"> * </span>
                <span class="fas fa-user"></span>
            </div>
        </div>
        @if($errors->has('name'))
        <div class="invalid-feedback">
            <strong>{{ $errors->first('name') }}</strong>
        </div>
        @endif
    </div>
    {{-- Employee Id field --}}
    <div class="input-group mb-3">
        <input type="text" name="employee_id" class="form-control {{ $errors->has('employee_id') ? 'is-invalid' : '' }}" value="{{ old('employee_id') }}" placeholder="Employee ID" autofocus>
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="text-danger" title="Required"> * </span>
                <span class="fas fa-id-card"></span>
            </div>
        </div>
        @if($errors->has('employee_id'))
        <div class="invalid-feedback">
            <strong>{{ $errors->first('employee_id') }}</strong>
        </div>
        @endif
    </div>

    {{-- ph_no field --}}
    <div class="input-group mb-3">
        <input type="tel" name="ph_no" class="form-control {{ $errors->has('ph_no') ? 'is-invalid' : '' }}" value="{{ old('ph_no') }}" placeholder="Office Phone (09777 ...)">
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="text-danger" title="Required"> * </span>
                <span class="fas fa-phone {{ config('adminlte.classes_auth_icon', '') }}"></span>
            </div>
        </div>
        @if($errors->has('ph_no'))
        <div class="invalid-feedback">
            <strong>{{ $errors->first('ph_no') }}</strong>
        </div>
        @endif
    </div>
    {{-- Email field --}}
    <div class="input-group mb-3">
        <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" value="{{ old('email') }}" placeholder="{{ __('adminlte::adminlte.email') }}">
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="fas fa-envelope {{ config('adminlte.classes_auth_icon', '') }}"></span>
            </div>
        </div>
        @if($errors->has('email'))
        <div class="invalid-feedback">
            <strong>{{ $errors->first('email') }}</strong>
        </div>
        @endif
    </div>
    <div class="form-group mb-3">
        <select class="form-control" id="branch_name" name="branch_id" required focus>
            <option value="" disabled selected>-- Please select branch --</option>
            @foreach($branches as $branch)
            <option value="{{$branch->id}}" {{ old('branch_id') == $branch->id ? 'selected': '' }} >{{ $branch->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group mb-3">
        <select name="dept_id" id="dept_name" class="form-control">
            <option value="0">-- Please select department --</option>
        </select>
    </div>
    <div class="form-group mb-3">
        <select class="form-control" id="mercate_name" name="mercate_id" required focus>
            <option value="" disabled selected>-- Please select MercatCategory --</option>
            @foreach($mercats as $mercat)
            <option value="{{$mercat->id}}" {{ old('mercate_id') == $mercat->id ? 'selected': '' }} >{{ $mercat->name }}</option>
            @endforeach
        </select>
    </div>
    {{-- Password field --}}
    <div class="input-group mb-3">
        <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="{{ __('adminlte::adminlte.password') }}">
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="text-danger" title="Required"> * </span>
                <span class="fas fa-lock {{ config('adminlte.classes_auth_icon', '') }}"></span>
            </div>
        </div>
        @if($errors->has('password'))
        <div class="invalid-feedback">
            <strong>{{ $errors->first('password') }}</strong>
        </div>
        @endif
    </div>
    {{-- Confirm password field --}}
    <div class="input-group mb-3">
        <input type="password" name="confirm-password" class="form-control {{ $errors->has('confirm-password') ? 'is-invalid' : '' }}" placeholder="{{ __('adminlte::adminlte.retype_password') }}">
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="text-danger" title="Required"> * </span>
                <span class="fas fa-lock {{ config('adminlte.classes_auth_icon', '') }}"></span>
            </div>
        </div>
        @if($errors->has('confirm-password'))
        <div class="invalid-feedback">
            <strong>{{ $errors->first('confirm-password') }}</strong>
        </div>
        @endif
    </div>
    {{-- Get OTP button --}}
    <button type="submit" class="btn btn-block btn-flat btn-primary">
        <span class="fas fa-mobile-alt"></span>
        {{ __('adminlte::adminlte.get_otp') }}
    </button>
    {{-- Register button 
        <button type="submit" class="btn btn-block btn-flat btn-primary">
            <span class="fas fa-user-plus"></span>
            {{ __('adminlte::adminlte.register') }}
    </button>
    --}}
</form>
@stop

@section('auth_footer')
<p class="my-0">
    <a href="{{ route('user_login') }}">
        {{ __('adminlte::adminlte.i_already_have_a_membership') }}
    </a>
</p>
@stop

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $('#branch_name').change(function() {
            var id = $(this).val();
            $('#dept_name').find('option').not(':first').remove();
            $.ajax({
                url: 'dept/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    var len = 0;
                    if (response.data != null) {
                        len = response.data.length;
                    }
                    if (len > 0) {
                        for (var i = 0; i < len; i++) {
                            var id = response.data[i].id;
                            var name = response.data[i].name;
                            var option = "<option value='" + id + "'>" + name + "</option>";
                            $("#dept_name").append(option);
                        }
                    }
                }
            })
        });
    });
</script>
@endsection