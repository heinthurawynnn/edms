@extends('adminlte::page')
@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Product Code</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ URL::previous() }}"> Back</a>
        </div>
    </div>
</div>

@foreach ($products as $product)
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Type:</strong>
            {{ $product_Code->type === 0 ? 'HIP' : 'Structure' }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Product Code No:</strong>
            {{ $product_Code->product_code_no }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Product Name:</strong>
            {{ $product_Code->product_name }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Category:</strong>
            {{ $product_Code->category_id ? $product_Code->categories->product_category_name : 'Uncategorized' }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Group:</strong>
            {{ $product_Code->group_id ? $product_Code->groups->product_group_name : 'Uncategorized' }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Pattern:</strong>
            {{ $product_Code->pattern_id ? $product_Code->patterns->product_pattern_name : 'Uncategorized' }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Design:</strong>
            {{ $product_Code->design_id ? $product_Code->designs->product_design_name : 'Uncategorized' }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Supplier:</strong>
            {{ $product_Code->supplier_id ? $product_Code->suppliers->vendor_name : 'Uncategorized' }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Brand:</strong>
            {{ $product_Code->brand_id ? $product_Code->brands->product_brand_name : 'Uncategorized' }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Unit:</strong>
            {{ $product_Code->unit_id ? $product_Code->units->product_unit_name : 'Uncategorized' }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Product Pack Flag:</strong>
            @if ($product_Code->product_pack_flag == 0)
            {{ 'S' }}
            @elseif($product_Code->product_pack_flag == 1)
            {{ 'P' }}
            @elseif($product_Code->product_pack_flag == 2)
            {{ 'G' }}
            @else
            {{ 'S' }}
            {{ 'P' }}
            {{ 'G' }}
            @endif
        </div>
    </div>
    @endforeach
</div>
@endsection