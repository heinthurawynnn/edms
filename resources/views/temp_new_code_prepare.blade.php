<!DOCTYPE html>
@extends('layouts.app')

@section('content')
<div class="row mb-3 ">
    <div class="col-md-12  text-center py-3 my-2 bg-light">
        <h1 class="text-uppercase align-center m-auto">Prepare New ERP Code </h1>
    </div>
    <div class="col-md-6 col-sm-12 col-lg-9 ">
        <a class="btn btn-info" href="{{ route('documents_list', Auth::id()) }}">Prepared Document List</a>

        <button type="button" id="modal_btn" class="btn btn-primary" >
            Create New Code
        </button>
        <form action="{{ route('product_Codes.store') }}" method="POST" enctype="multipart/form-data" id='pcode' class="m-5 p-3 bg-light d-none">

            @csrf

            <div class="row">
                <div class="offset-xs-10 offset-sm-10  offset-md-10 col-xs-2 col-sm-2 col-md-2">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Save <i class="fa fab fa-save"></i></button>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <label class="control-label col-sm-4 pull-left"> Name :</label>

                        <input type="text" name="product_name" class="form-control" value="{{ old('product_name') }}" placeholder="Name">
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <label class="control-label col-sm-4 pull-left"> Prodcut Code:</label>
                        <input type="text" name="product_code_no" class="form-control" value="{{ old('product_code_no') }}" placeholder="Barcode">
                    </div>
                </div>

                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label col-sm-4 pull-left"> Product Type <span class="text-red">*</span>:</label>
                        <select class="form-control" name="type" required focus>
                            <option value="">{{ '--HIP ? Structure--' }}</option>
                            @if (!empty(old('type')) && old('type') == 0)
                            <option value="0" selected>{{ 'HIP' }}</option>
                            <option value="1">{{ 'STRUCTURE' }}</option>
                            @elseif(!empty(old('type')) && old('type') == 1)

                            <option value="0">{{ 'HIP' }}</option>
                            <option value="1" selected>{{ 'STRUCTURE' }}</option>
                            @else

                            <option value="0">{{ 'HIP' }}</option>
                            <option value="1">{{ 'STRUCTURE' }}</option>
                            @endif
                        </select>
                    </div>
                </div>

                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label col-sm-4 pull-left"> Category Name <span class="text-red">*</span>:</label>
                        <select class="form-control" id='category_id' name="{{'category_id'}}" required focus>
                            <option value="">{{ '--Select One Category--' }}</option>
                            @if($categories != null)
                            @foreach ($categories as $ln)
                            <option value="{{$ln->id}}" {{ old('category_id') == $ln->id ? 'selected': '' }}>{{ $ln->product_category_name }}</option>


                            @endforeach
                            @elseif ($category != null)
                            <option value="{{$category->id}}" {{ old('category_id') == $category->id ? 'selected': '' }}>{{ $category->product_category_name }}</option>
                            @endif
                        </select>
                    </div>

                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label col-sm-4 pull-left"> Group Name <span class="text-red">*</span>:</label>
                        <select class="form-control" id='group_id' name="{{'group_id'}}" required focus>
                            <option value="">{{ '--Select One Group--' }}</option>
                        </select>
                    </div>

                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label col-sm-4 pull-left"> Pattern Name <span class="text-red">*</span>:</label>
                        <select class="form-control" id='pattern_id' name="{{'pattern_id'}}" required>
                            <option value="">{{ '--Select One Pattern--' }}</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label col-sm-4 pull-left"> Design Name <span class="text-red">*</span>:</label>
                        <select class="form-control" id='design_id' name="{{'design_id'}}" required>
                            <option value="">{{ '--Select One Design--' }}</option>

                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label col-sm-4 pull-left"> Unit Name <span class="text-red">*</span>:</label>
                        <select class="form-control" required name="{{'unit_id'}}" required>
                            <option value="">{{ '--Select One Unit--' }}</option>
                            @foreach ($units as $ln)
                            <option value="{{$ln->id}}">{{ $ln->product_unit_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label col-sm-4 pull-left"> Vendor Name <span class="text-red">*</span>:</label>
                        <select class="form-control" name="{{'supplier_id'}}" required>
                            <option value="">{{ '--Select One Vendor--' }}</option>
                            @foreach ($vendors as $ln)
                            <option value="{{$ln->id}}">{{ $ln->vendor_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label col-sm-4 pull-left"> Brand Name <span class="text-red">*</span>:</label>
                        <select class="form-control" name="{{'brand_id'}}" required>
                            <option value=""> {{ '--Select One Brand--' }}</option>
                            @foreach ($brands as $ln)
                            <option value="{{$ln->id}}">{{ $ln->product_brand_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label col-sm-4 pull-left"> Product Pack Flag <span class="text-red">*</span>:</label>
                        <select class="form-control" name="product_pack_flag" required focus>
                            <option value="">{{ '--S ? P ? G--' }}</option>
                            @if (!empty(old('product_pack_flag')) && old('product_pack_flag') == 0)
                            <option value="0" selected>{{ 'S' }}</option>
                            <option value="1">{{ 'P' }}</option>
                            <option value="2">{{ 'G' }}</option>
                            @elseif(!empty(old('product_pack_flag')) && old('product_pack_flag') == 1)

                            <option value="0">{{ 'S' }}</option>
                            <option value="1" selected>{{ 'P' }}</option>
                            <option value="2">{{ 'G' }}</option>
                            @elseif(!empty(old('product_pack_flag')) && old('product_pack_flag') == 2)

                            <option value="0">{{ 'S' }}</option>
                            <option value="1">{{ 'P' }}</option>
                            <option value="2" selected>{{ 'G' }}</option>
                            @else

                            <option value="0">{{ 'S' }}</option>
                            <option value="1">{{ 'P' }}</option>
                            <option value="2">{{ 'G' }}</option>

                            @endif
                        </select>
                    </div>
                </div>
            </div>
        </form>


        <form action="{{ route('doc_store') }}" method="POST" enctype="multipart/form-data">
            <!-- {{ csrf_field() }} -->
            @csrf
            <div class="bg-light p-3">
                <div class="row mb-3">
                    <div class="col-3">Document No</div>

                    <div class="col-9">
                        <input type="text" name="document_no" value="{{$document_no}}" />
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3">User Name</div>
                    <div class="col-9"> <input type="hidden" name="prepared_by" value="{{ Auth::id() }}" />
                        {{ Auth::user()->name }}
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3">Document Date</div>
                    <div class="col-9"> {{ now() }}</div>
                    <!-- <div class="col-6"><input class="form-control" type="date" name="doc_date" id="doc_date" value="2020-02-03" min="2000-01-01" max="2020-02-12" /></div> -->
                </div>
                <div class="row mb-3 text-muted">
                    <div class="col-3">Approved by </div>
                    <div class="col-9">-</div>
                </div>
                <div class="row mb-3 text-muted">
                    <div class="col-3">Approved Date </div>
                    <div class="col-9">-</div>
                </div>

                <input type="hidden" name="pcode_id" value='{{"{5,7,3}"}}' />

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">{{ 'Save' }}</button>

                </div>
            </div>
        </form>

    </div>

</div>

@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $("#modal_btn").click(function() {

             
            $("#pcode").removeClass("d-none");
            $("#pcode").addClass("d-block");
 
            $('#category_id').click(function() {
                var id = $(this).val();
                $('#group_id').find('option').not(':first').remove();
                $.ajax({
                    url: 'cat/' + id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response) {
                        var len = 0;
                        if (response.data != null) {
                            len = response.data.length;
                        }

                        if (len > 0) {
                            for (var i = 0; i < len; i++) {
                                var id = response.data[i].product_group_id;
                                var name = response.data[i].product_group_name;
                                // console.log(name);
                                var option = "<option value='" + id + "'>" + name + "</option>";

                                $("#group_id").append(option);
                            }
                        }
                    }
                })
            });
            $('#group_id').change(function() {
                var id = $(this).val();
                $('#pattern_id').find('option').not(':first').remove();
                $.ajax({
                    url: 'gp/' + id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response) {
                        var len = 0;
                        if (response.data != null) {
                            len = response.data.length;
                        }

                        if (len > 0) {
                            for (var i = 0; i < len; i++) {
                                var id = response.data[i].product_pattern_id;
                                var name = response.data[i].product_pattern_name;
                                // console.log(name);
                                var option = "<option value='" + id + "'>" + name + "</option>";
                                $("#pattern_id").append(option);
                            }
                        }
                    }
                })
            });
            $('#pattern_id').change(function() {
                var id = $(this).val();
                $('#design_id').find('option').not(':first').remove();
                $.ajax({
                    url: 'ptn/' + id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response) {
                        var len = 0;
                        if (response.data != null) {
                            len = response.data.length;
                        }

                        if (len > 0) {
                            for (var i = 0; i < len; i++) {
                                var id = response.data[i].product_design_id;
                                var name = response.data[i].product_design_name;
                                // console.log(name);
                                var option = "<option value='" + id + "'>" + name + "</option>";
                                $("#design_id").append(option);
                            }
                        }
                    }
                })
            });
        });
    });
</script>
@endsection