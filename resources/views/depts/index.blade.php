@extends('adminlte::page')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Departments</h2>
        </div>
        <div class="pull-right mb-3">
            @can('dept-create')
            <a class="btn btn-success" href="{{ route('depts.create') }}"> Add New Department</a>
            @endcan
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Branch Name</th>
        <th>Dept Name</th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($depts as $dept)
    <tr>
        <td>{{ ++$i }}</td>
        <td>
        {{ isset($dept->branch_id) ? $dept->branches->name: '-'}}</td>
        <td>{{ $dept->name }}</td>
        <td>
            <form action="{{ route('depts.destroy',$dept->id) }}" method="POST">
                <a class="btn btn-info" href="{{ route('depts.show',$dept->id) }}">Show</a>
                @can('dept-edit')
                <a class="btn btn-primary" href="{{ route('depts.edit',$dept->id) }}">Edit</a>
                @endcan
                @csrf
                @method('DELETE')
                @can('dept-delete')
                <button type="submit" class="btn btn-danger">Delete</button>
                @endcan
            </form>
        </td>
    </tr>
    @endforeach
</table>
{!! $depts->links() !!}

@endsection