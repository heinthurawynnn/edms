@extends('adminlte::page')
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2></h2>
        </div>
        <div class="pull-right mb-3">
            @can('pcode-create')
            <a class="btn btn-success" href="{{ route('documents.create') }}"> Create New Document</a>
            @endcan

        </div>

    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Document No</th>
        <th>Product Code</th>
        <th>Status</th>
        <th>Prepared By</th>
        <th>Prepared At</th>
        <th>Checked By</th>
        <th>Checked At</th>
        <th>Approved By</th>
        <th>Approved At</th>
        <th>Exported By</th>
        <th>Exported At</th>
        <th width="380px">Action</th>
    </tr>
    @foreach ($documents as $document)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $document->document_no}}</td>
        <td>{{ $document->pcode_id }}</td>
        <td>{{ $document->status }}</td>
        <td>{{ $document->prepared->name }}</td>
        <td>{{ $document->prepared_at }}</td>
        <td>{{ $document->checked_by ? $document->checked->name : "" }}</td>
        <td>{{ $document->checked_at }}</td>
        <td>{{ $document->approved_by ? $document->approved->name : "" }}</td>
        <td>{{ $document->approved_at }}</td>
        <td>{{ $document->exported_by ? $document->exported->name : ""}}</td>
        <td>{{ $document->exported_at}}</td>
        <td>
            <form action="{{ route('documents.destroy',$document->id) }}" method="POST">
                <a class="btn btn-info" href="{{ route('documents.show',$document->id) }}">Show</a>
                @can('document-edit')
                <a class="btn btn-primary" href="{{ route('documents.edit',$document->id) }}">Edit</a>
                @endcan
                @csrf
                @method('DELETE')
                @can('document-delete')
                <button type="submit" class="btn btn-danger">Delete</button>
                @endcan
            </form>
        </td>
    </tr>
    @endforeach
</table>
@endsection