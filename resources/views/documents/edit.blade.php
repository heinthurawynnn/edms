@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Document</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('documents.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('documents.update',$document->id) }}" method="POST">
    	@csrf
        @method('PUT')
         <div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Product Pattern Id:</strong>
		            <input type="text" name="document_no" value="{{ $document->document_no }}" class="form-control" placeholder="Document No">
		        </div>
		    </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Product Code ID:</strong> 
		            <input type="text" name="pcode_id" value="{{ $document->pcode_id }}" class="form-control" placeholder="Product Code ID">
		        </div>
		    </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Prepared By:</strong>
		            <input type="text" name="prepared_by" value="{{ $document->prepared->name }}" class="form-control" placeholder="Prepared By">
		        </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Prepared At:</strong>
		            <input type="text" name="prepared_at" value="{{ $document->prepared_at }}" class="form-control" placeholder="Prepared At">
		        </div>
		    </div>
			<div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Checked By:</strong>
		            <input type="text" name="checked_by" value="{{ $document->checked->name }}" class="form-control" placeholder="Checked By">
		        </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Checked At:</strong>
		            <input type="text" name="checked_at" value="{{ $document->checked_at }}" class="form-control" placeholder="Checked At">
		        </div>
		    </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Approved By:</strong> 
		            <input type="text" name="approved_by" value="{{ $document->approved_by ? $document->approved->name : " " }}" class="form-control" placeholder="Approved By">
		        </div>
		    </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Approved At:</strong>
		            <input type="text" name="approved_at" value="{{ $document->approved_at }}" class="form-control" placeholder="Approved At">
		        </div>
		    </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Exported By:</strong> 
		            <input type="text" name="exported_by" value="{{ $document->exported_by ? $document->exported->name : "" }} " class="form-control" placeholder="Exported By">
		        </div>
		    </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Exported At:</strong>
		            <input type="text" name="exported_at" value="{{ $document->exported_at }}" class="form-control" placeholder="Exported At">
		        </div>
		    </div>
		    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
		      <button type="submit" class="btn btn-primary">Submit</button>
		    </div>
		</div>
    </form>
@endsection