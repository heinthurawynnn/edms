@extends('adminlte::page')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Document</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('documents.index') }}"> Back</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Product Pattern Id:</strong>
            {{ $document->document_no }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Product Code ID:</strong>
            {{ $document->pcode_id }}
            @php
            $code_ids = $document->pcode_id;
            $code_ids = explode (",", $code_ids);  
            @endphp
            @foreach($code_ids as $code_id)
            <a class="btn btn-info" href="{{ route('product_Codes.show',$code_id) }}">Show {{$code_id}}</a>
            @endforeach
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Prepared By:</strong>
            {{ $document->prepared->name }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Prepared At:</strong>
            {{ $document->prepared_at }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Checked By:</strong>
            {{ $document->checked->name }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Checked At:</strong>
            {{ $document->checked_at }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Approved By:</strong>
            {{ $document->approved_by ? $document->approved->name : "" }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Approved At:</strong>
            {{ $document->approved_at }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Exported By:</strong>
            {{ $document->exported_by ? $document->exported->name : ""}}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Exported At:</strong>
            {{ $document->exported_at }}
        </div>
    </div>
</div>
@endsection