@extends('adminlte::page')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Product Pattern</h2>
        </div>
        <div class="pull-right mb-3">
            @can('product-pattern-create')
            <a class="btn btn-success" href="{{ route('product_Patterns.create') }}"> Add New Pattern</a>
            @endcan
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif


<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Product Pattern Id</th>
        <th>Product Pattern Code</th>
        <th>Product Pattern Name</th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($product_Patterns as $product_Pattern)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $product_Pattern->product_pattern_id }}</td>
        <td>{{ $product_Pattern->product_pattern_code }}</td>
        <td>{{ $product_Pattern->product_pattern_name }}</td>
      
        <td>
            <form action="{{ route('product_Patterns.destroy',$product_Pattern->id) }}" method="POST">
                <a class="btn btn-info" href="{{ route('product_Patterns.show',$product_Pattern->id) }}">Show</a>
                @can('product-pattern-edit')
                <a class="btn btn-primary" href="{{ route('product_Patterns.edit',$product_Pattern->id) }}">Edit</a>
                @endcan


                @csrf
                @method('DELETE')
                @can('product-pattern-delete')
                <button type="submit" class="btn btn-danger">Delete</button>
                @endcan
            </form>
        </td>
    </tr>
    @endforeach
</table>


{!! $product_Patterns->links() !!}


@endsection