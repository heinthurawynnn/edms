@extends('layouts.app')
@section('content')
<div class="container bg-light">
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Departments</h2>
        </div>
        <div class="pull-right mb-3">
            @can('field-create')
            <a class="btn btn-success" href="{{ route('periods.create') }}"> Add New Time Field</a>
            @endcan
        </div>
        
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <tr>
        <th>ID</th>
        <th>Name</th>
    </tr>
    @foreach ($periods as $period)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $period->time }}</td>
    </tr>
    @endforeach
</table>
</div>
@endsection