@extends('layouts.app')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Time</title>
    <meta charset="utf-8">
</head>
<body>
    <div class="row">
    </div>
    <div class="container mt-4">
        @if(session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="card">
            <div class="card-header text-center font-weight-bold">
               Time
            </div>
            <div class="card-body">
                <form action="{{ route('periods.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Time</label>
                        <input type="time" id="time1" name="time1" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Time</label>
                        <input type="time" id="time2" name="time2" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
@endsection