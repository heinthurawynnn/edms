@extends('layouts.app')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Customer</title>
    <meta charset="utf-8">
</head>
<body>
    <div class="row">
    </div>
    <div class="container mt-4">
        @if(session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="card">
            <div class="card-header text-center font-weight-bold">
                Customer Record Data
            </div>
            <div class="card-body">
                <form action="{{ route('customers.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <strong>Branch Code :</strong>
                        <select class="form-control" id="branch_code" name="branch_code" required focus>
                            <option value="" disabled selected>-- Please select branch --</option>
                            @foreach($branches as $branch)
                            <option value="{{$branch->branch_code}}" {{ old('branch_code') == $branch->id ? 'selected': '' }}>{{ $branch->branch_code }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <strong>Branch Code :</strong>
                        <select class="form-control" id="branch_code" name="branch_code" required focus>
                            <option value="" disabled selected>-- Please select Field --</option>
                            @foreach($branches as $branch)
                            <option value="{{$branch->branch_code}}" {{ old('branch_code') == $branch->id ? 'selected': '' }}>{{ $branch->branch_code }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <strong>Branch Code :</strong>
                        <select class="form-control" id="branch_code" name="branch_code" required focus>
                            <option value="" disabled selected>-- Please select Time Period --</option>
                            @foreach($branches as $branch)
                            <option value="{{$branch->branch_code}}" {{ old('branch_code') == $branch->id ? 'selected': '' }}>{{ $branch->branch_code }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <strong>Branch Code :</strong>
                        <select class="form-control" id="branch_code" name="branch_code" required focus>
                            <option value="" disabled selected>-- Please select branch --</option>
                            @foreach($branches as $branch)
                            <option value="{{$branch->branch_code}}" {{ old('branch_code') == $branch->id ? 'selected': '' }}>{{ $branch->branch_code }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Date Client</label>
                        <input type="date" id="date_client" name="date_client" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Number Of Customer</label>
                        <input type="text" id="number_of_customer" name="number_of_customer" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
@endsection