@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Show unit</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('units.index') }}"> Back</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Unit Code:</strong>
                {{ $unit->product_unit_code }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Unit Name:</strong>
                {{ $unit->product_unit_name }}
            </div>
        </div>
    </div>
@endsection