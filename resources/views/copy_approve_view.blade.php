<!DOCTYPE html>
@extends('layouts.app')

@section('content')

@php
$pcount = count($products);
$pcode_ids = "";
@endphp

<div class="row mb-3">
    <div class="col-md-12  text-center py-3 my-2 bg-light">
        <h1 class="text-uppercase align-center m-auto">Approve Document </h1>
    </div>
</div>
<div class="row mb-3 ">
    <div class="col-md-6 col-sm-12 col-lg-3">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="bg-light p-3 my-5 card">
            <div class="row pb-3">
                <div class="col text-right">
                    <form action="{{ route('doc_destroy',$document->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        @can('reject-manager')
                        <button type="submit" class="btn btn-danger text-right ">Reject Delete <i class="fas fa-trash"></i></button>
                        @endcan
                    </form>
                </div>
                <form action="{{ route('approve_store',$document->id) }}" method="POST">
                    @csrf
                    <div class="col text-right">
                        @can('approve-manager')
                        <button type="submit" id="save_doc" class="btn btn-success text-right {{ $pcount == 0 ? 'd-none': ''}} ">{{ 'Approve Doc' }} <i class="fas fa-save"></i> <i class="fas fa-file-word"></i></button>
                        @endcan
                    </div>
            </div>
            <div class="row mb-3">
                <div class="col-6">Document No :</div>
                <div class="col-6">
                    <input type="hidden" name="doc_id" value="{{$document->id}}" />
                    <input type="text" disabled name="document_no" value="{{$document->document_no}}" />
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-6">Requested by :</div>
                <div class="col-6">
                    {{$document->prepared->name}}
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-6">Approved by :</div>
                <div class="col-6">
                    <input type="hidden" name="approved_by" value="{{ Auth::id() }}" />
                    {{ Auth::user()->name }}
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-6">Document Approved Date</div>
                <div class="col-6"> {{ now() }}</div>
            </div>
            <input type="hidden" name="pcode_id" id="pcode_ids" />
            </form>
        </div>
    </div>
</div>
<table class="table table-striped table-hover table-bordered bg-white {{ $pcount == 0 ? 'd-none': ''}} ">
    <tr>
        <th>No</th>
        <th>Type</th>
        <th>Product Code No</th>
        <th>Product Name</th>
        <th>Category</th>
        <th>Group</th>
        <th>Pattern</th>
        <th>Design</th>
        <th>Supplier</th>
        <th>Brand</th>
        <th>Unit</th>
        <th>Product Pack Flag</th>
        <th width="280px">Action</th>
    </tr>

    @foreach ($products as $product)
    <tr>
        <td colspan=13>
            <form action="{{ route('pcode_store') }}" method="POST" enctype="multipart/form-data" id="" class="mb-5 p-3 bg-light ">
                @csrf
                <div class="row">
                    <div class="offset-xs-10 offset-sm-10  offset-md-10 col-xs-2 col-sm-2 col-md-2">
                        <div class="form-group">
                            <input type="hidden" name="doc_no" id="doc_id" value="{{$document->id}}">
                            <button type="submit" class="btn btn-primary ">Save <i class="fas fa-save "></i></button>
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Product Code Type <span class="text-danger">*</span>:</label>
                            <select class="form-control" name="product_pack_flag" required focus>
                                @if (!empty($product->product_pack_flag) && $product->product_pack_flag == 0)
                                <option value="0" selected>{{ 'Product Code' }}</option>
                                <option value="1">{{ 'FOC' }}</option>
                                @if (!empty($product->product_pack_flag) && $product->product_pack_flag == 1)

                                <option value="0">{{ 'Product Code' }}</option>
                                <option value="1" selected>{{ 'FOC' }}</option>
                                @else

                                <option value="0">{{ 'Product Code' }}</option>
                                <option value="1">{{ 'FOC' }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="form-group">

                            <label class="control-label"> Prodcut Code:</label>
                            <div class="row">
                                <p id="pcode_gen" class="col-3 btn btn-primary ">Generate <i class="fas fa-reload "></i></p><input type="number" min=0 name="product_code_no" autocomplete="off" class="form-control col-9" id="barcode" value="{{ old('product_code_no') }}" placeholder="Barcode">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="form-group">
                            <label class="control-label"> Name <span class="text-danger">*</span>:</label>

                            <input type="text" name="product_name" class="form-control" value="{{ old('product_name') }}" placeholder="Name" required focus autocomplete="off">
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <label class="control-label"> Product Type <span class="text-danger">*</span>:</label>
                            <select class="form-control" name="type" required focus>
                                <option value="">{{ '--HIP ? Structure--' }}</option>
                                @if (!empty(old('type')) && old('type') == 0)
                                <option value="0" selected>{{ 'HIP' }}</option>
                                <option value="1">{{ 'STRUCTURE' }}</option>
                                @elseif(!empty(old('type')) && old('type') == 1)
                                <option value="0">{{ 'HIP' }}</option>
                                <option value="1" selected>{{ 'STRUCTURE' }}</option>
                                @else
                                <option value="0">{{ 'HIP' }}</option>
                                <option value="1">{{ 'STRUCTURE' }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Grade <span class="text-danger">*</span>:</label>
                            <select class="form-control" name="type" required focus>
                                <option value="">{{ '--Local ? Import--' }}</option>
                                @if (!empty(old('type')) && old('type') == 0)
                                <option value="0" selected>{{ 'Local' }}</option>
                                <option value="1">{{ 'Import' }}</option>
                                @elseif(!empty(old('type')) && old('type') == 1)
                                <option value="0">{{ 'Local' }}</option>
                                <option value="1" selected>{{ 'Import' }}</option>
                                @else
                                <option value="0">{{ 'Local' }}</option>
                                <option value="1">{{ 'Import' }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <label class="control-label"> Category <span class="text-danger">*</span>:</label>
                            <select class="form-control" id='category_id' name="{{'category_id'}}" required focus>
                                <option value="">{{ '--Select One Category--' }}</option>
                                @if($categories != null)
                                @foreach ($categories as $ln)
                                <option value="{{$ln->id}}" {{ old('category_id') == $ln->id ? 'selected': '' }}>{{ $ln->product_category_code }}</option>
                                @endforeach
                                @elseif ($category != null)
                                <option value="{{$category->id}}" {{ old('category_id') == $category->id ? 'selected': '' }}>{{ $category->product_category_code }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <label class="control-label"> Group <span class="text-danger">*</span>:</label>
                            <select class="form-control" id='group_id' name="{{'group_id'}}" required focus>
                                <option value="">{{ '--Select One Group--' }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <label class="control-label"> Pattern <span class="text-danger">*</span>:</label>
                            <select class="form-control" id='pattern_id' name="{{'pattern_id'}}" required>
                                <option value="">{{ '--Select One Pattern--' }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <label class="control-label"> Design <span class="text-danger">*</span>:</label>
                            <select class="form-control" id='design_id' name="{{'design_id'}}" required>
                                <option value="">{{ '--Select One Design--' }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <label class="control-label"> Unit <span class="text-danger">*</span>:</label>
                            <div class="form-group">
                                <input type="hidden" name="unit_id" id="product_unit_id">
                                <input type="text" id="product_unit_name" class="form-control input-lg" autocomplete="off" placeholder="Enter One Unit " />
                                <div id="product_unit_name_list"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <label class="control-label"> Supplier <span class="text-danger">*</span>:</label>
                            <div class="form-group">
                                <input type="hidden" name="supplier_id" id="product_vendor_id">
                                <input type="text" id="product_vendor_name" class="form-control input-lg" autocomplete="off" placeholder="Enter One Supplier " />
                                <div id="product_vendor_name_list"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <label class="control-label"> Brand <span class="text-danger">*</span>:</label>
                            <div class="form-group">
                                <input type="hidden" name="brand_id" id="product_brand_id">

                                <input type="text" id="product_brand_name" class="form-control input-lg" autocomplete="off" placeholder="Enter One Brand" />
                                <div id="product_brand_name_list"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </td>
    </tr>
    <tr>
        <td>{{++$i}}</a></td>
        <td>{{ $product->type === 0 ? 'HIP' : 'Structure' }}</td>
        <td>{{ $product->product_code_no }}</td>
        <td>{{ $product->product_name }}</td>
        <td>{{ $product->category_id ? $product->categories->product_category_name : 'Uncategorized' }}</td>
        <td>{{ $product->group_id ? $product->groups->product_group_name : 'Uncategorized' }}</td>
        <td>{{ $product->pattern_id ? $product->patterns->product_pattern_name : 'Uncategorized' }}</td>
        <td>{{ $product->design_id ? $product->designs->product_design_name : 'Uncategorized' }}</td>
        <td>{{ $product->supplier_id ? $product->suppliers->vendor_name : 'Uncategorized' }}</td>
        <td>{{ $product->brand_id ? $product->brands->product_brand_name : 'Uncategorized' }}</td>
        <td>{{ $product->unit_id ? $product->units->product_unit_name : 'Uncategorized' }}</td>

        <td> @if ($product->product_pack_flag == 0)
            {{ 'G Product Code' }}
            @elseif($product->product_pack_flag == 1)
            {{ 'GP' }}
            @elseif($product->product_pack_flag == 2)
            {{ 'FOC' }}
            @else
            {{ 'S' }}
            {{ 'P' }}
            {{ 'G' }}
            @endif
        </td>
        <td class="d-flex">
            @php
            $pcode_ids = $pcode_ids .",". $product->id;
            @endphp
            <p class="btn btn-primary mr-3 edit-btn" id="{{$product->id}}">Edit <i class="fas fa-edit"></i></a>

            <form action="{{ route('pcode_destroy',$product->id) }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete <i class="fas fa-trash"></i></button>
            </form>
        </td>
    </tr>
    @endforeach
</table>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function() {

        var doc_id = $("#doc_id").val();
        var pcode_ids = '<?php echo $pcode_ids; ?>';
        $("#pcode_ids").val(pcode_ids);
        $(".edit-btn").click(function() {
            $("#pcode").removeClass("d-none");
            $("#pcode").addClass("d-block");
        });


        <?php if ($errors->any()) { ?>
            $("#pcode").removeClass("d-none");
            $("#pcode").addClass("d-block");
            $("#modal_btn").addClass("d-none");
        <?php } ?>
    });
</script>
@endsection