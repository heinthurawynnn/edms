<!DOCTYPE html>
@extends('layouts.app')
@section('content')
<div class="row mb-3 ">
    <div class="col-md-12  text-center py-3 my-2 bg-light">
        <h1 class="text-uppercase align-center m-auto">Edit New Code (ERP) </h1>
    </div>
    <div class="col-md-6 col-sm-12 col-lg-9 bg-white">
        <form action="{{ route('manager_product_codes_update',$product->id) }}" method="POST">
            @csrf
            <div class="row mt-3">
                <div class="offset-xs-10 offset-sm-10 offset-md-10 col-xs-2 col-sm-2 col-md-2">
                    <div class="form-group text-right">
                        <input type="hidden" name="product_id" value='{{$product->id}}'>
                        <button type="submit" class="btn btn-primary ">Save <i class="fas fa-save "></i></button>
                    </div>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2">
                    <label class="control-label"> Product Code Type <span class="text-danger">*</span>:</label>
                    <select class="form-control" name="product_pack_flag">
                        @if ($product->product_pack_flag == 0)
                        <option value="0" selected>{{ 'Product Code' }}</option>
                        <option value="1">{{ 'FOC' }}</option>
                        @elseif($product->product_pack_flag == 1)
                        <option value="0">{{ 'Product Code' }}</option>
                        <option value="1" selected>{{ 'FOC' }}</option>
                        @else
                        <option value="0">{{ 'Product Code' }}</option>
                        <option value="1">{{ 'FOC' }}</option>
                        @endif
                    </select>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Prodcut Code:<span class="text-danger">*</span>:</label>
                        <p class="form-control" readonly>{{ $product->product_code_no }}</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <label class="control-label"> Name: <span class="text-danger">*</span>:</label>
                        <input type="text" name="product_name" value="{{ $product->product_name }}" class="form-control" autofocus placeholder="Name">
                    </div>
                </div>

                <div class="col-xs-4 col-sm-4 col-md-4">
                    <label class="control-label col-sm-4 pull-left"> Product Type {{$product_Code->type}} </label>
                    <select class="form-control" name="type">
                        @if ($product->type == 0)
                        <option value="0" selected>{{ 'HIP' }}</option>
                        <option value="1">{{ 'STRUCTURE' }}</option>
                        @elseif($product->type == 1)
                        <option value="0">{{ 'HIP' }}</option>
                        <option value="1" selected>{{ 'STRUCTURE' }}</option>
                        @else
                        <option value="0">{{ 'HIP' }}</option>
                        <option value="1">{{ 'STRUCTURE' }}</option>
                        @endif
                    </select>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <label class="control-label col-sm-4 pull-left"> Grade</label>
                    <select class="form-control" name="product_grade_id">
                        @if ($product->product_grade_id == 0)
                        <option value="0" selected>{{ 'Local' }}</option>
                        <option value="1">{{ 'Import' }}</option>
                        @elseif($product->type == 1)
                        <option value="0">{{ 'Local' }}</option>
                        <option value="1" selected>{{ 'Import' }}</option>
                        @else
                        <option value="0">{{ 'Local' }}</option>
                        <option value="1">{{ 'Import' }}</option>
                        @endif
                    </select>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Category <span class="text-danger">*</span>:</label>
                        <select class="form-control" id='category_id' name="{{'category_id'}}" required focus>
                            <option value="">{{ '--Select One Category--' }}</option>
                            @if($categories != null)
                            @foreach ($categories as $ln)
                            <option value="{{$ln->id}}" {{ $product->category_id == $ln->id ? 'selected': '' }}>{{ $ln->product_category_code ." | ".  $ln->product_category_name }}</option>
                            @endforeach
                            @elseif ($category != null)
                            @foreach ($category as $category_item)
                            <option value="{{$category_item->product_category_id}}" {{  $product->category_id  == $category_item->id ? 'selected': '' }}>{{ $category_item->product_category_code ." | ".  $category_item->product_category_name  }}</option>
                            @endforeach
                            @endif
                        </select>
                      
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Group <span class="text-danger">*</span>:</label>
                        <select class="form-control" id='group_id' name="{{'group_id'}}" required focus>
                            <option value="">{{ '--Select One Group--' }}</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Pattern <span class="text-danger">*</span>:</label>
                        <select class="form-control" id='pattern_id' name="{{'pattern_id'}}" required>
                            <option value="">{{ '--Select One Pattern--' }}</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Design <span class="text-danger">*</span>:</label>
                        <select class="form-control" id='design_id' name="{{'design_id'}}" required>
                            <option value="">{{ '--Select One Design--' }}</option>
                        </select>
                    </div>
                </div>
             
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label col-sm-4 pull-left"> Unit Name <span class="text-red">*</span>:</label>
                        <select class="form-control" name="{{'unit_id'}}">
                            @foreach ($units as $ln)
                            <option value="{{$ln->id}}" {{ ($ln->id == $product->unit_id  ? 'selected' : '') }}>{{ $ln->product_unit_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label col-sm-4 pull-left"> Supplier <span class="text-red">*</span>:</label>
                        <select class="form-control" name="{{'supplier_id'}}">
                            @foreach ($suppliers as $supplier)
                            <option value="{{$supplier->id}}" {{ ($supplier->id == $product->supplier_id  ? 'selected' : '') }}>{{ $supplier->vendor_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label col-sm-4 pull-left"> Brand <span class="text-red">*</span>:</label>
                        <select class="form-control" name="{{'brand_id'}}">
                            @foreach ($brands as $brand)
                            <option value="{{$brand->id}}" {{ ($brand->id == $product->brand_id  ? 'selected' : '') }}>{{ $brand->product_brand_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-6 col-sm-12 col-lg-3">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

    </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@endsection


@section('js')
<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }
</style>
<script type="text/javascript">
    $(document).ready(function() {

        $('#category_id').change(function() {
            var id = $(this).val();
            console.log(id);
            $('#group_id').find('option').not(':first').remove();
            $.ajax({
                url: '/product_Codes/cat/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    console.log(response.data);
                    var len = 0;
                    if (response.data != null) {
                        len = response.data.length;
                    }
                    if (len > 0) {
                        for (var i = 0; i < len; i++) {
                            var id = response.data[i].product_group_id;
                            var name = response.data[i].product_group_name + " | " + response.data[i].product_group_code;
                            // console.log(name);
                            var option = "<option value='" + id + "'>" + name + "</option>";
                            $("#group_id").append(option);
                        }
                    }
                }
            })
        });
        $('#group_id').change(function() {
            var id = $(this).val();
            $('#pattern_id').find('option').not(':first').remove();
            $.ajax({
                url: '/product_Codes/gp/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    var len = 0;
                    if (response.data != null) {
                        len = response.data.length;
                    }
                    if (len > 0) {
                        for (var i = 0; i < len; i++) {
                            var id = response.data[i].product_pattern_id;
                            var name = response.data[i].product_pattern_name + " | " + response.data[i].product_pattern_code;
                            // console.log(name);
                            var option = "<option value='" + id + "'>" + name + "</option>";
                            $("#pattern_id").append(option);
                        }
                    }
                }
            })
        });
        $('#pattern_id').change(function() {
            var id = $(this).val();
            $('#design_id').find('option').not(':first').remove();
            $.ajax({
                url: '/product_Codes/ptn/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    var len = 0;
                    if (response.data != null) {
                        len = response.data.length;
                    }
                    if (len > 0) {
                        for (var i = 0; i < len; i++) {
                            var id = response.data[i].product_design_id;
                            var name = response.data[i].product_design_code;
                            var name = response.data[i].product_design_name + " | " + response.data[i].product_design_code;
                            var option = "<option value='" + id + "'>" + name + "</option>";
                            $("#design_id").append(option);
                        }
                    }
                }
            })
        });
    });
</script>
@endsection