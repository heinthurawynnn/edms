<!DOCTYPE html>
@extends('layouts.app')
@section('content')
@php
$pcount = count($products);
$pcode_ids = "";
@endphp
<div class="row">
    <div class="col-md-12  text-center py-3 my-2 bg-light">
        <h1 class="text-uppercase align-center m-auto">Prepare New Code (ERP) </h1>
    </div>
    <div class="col-md-6 col-sm-12 col-lg-9 ">
        <form action="{{ route('pcode_store') }}" method="POST" enctype="multipart/form-data" id="pcode" class="mb-5 p-3 bg-light d-none ">
            @csrf
            <div class="row">
                <div class="offset-xs-10 offset-sm-10  offset-md-10 col-xs-2 col-sm-2 col-md-2">
                    <div class="form-group">
                        <input type="hidden" name="doc_no" id="doc_id" value="{{$doc_id}}">
                        <button type="submit" class="btn btn-primary ">Save <i class="fas fa-save "></i></button>
                    </div>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2">
                    <div class="form-group">
                        <label class="control-label">Product Type <span class="text-danger">*</span>:</label>
                        <select class="form-control" name="product_pack_flag" required focus>
                            <option value="">{{ '--Product Code ? FOC--' }}</option>
                            @if (!empty(old('product_pack_flag')) && old('product_pack_flag') == 0)
                            <option value="0" selected>{{ 'Product Code' }}</option>
                            <option value="1">{{ 'FOC' }}</option>
                            @elseif(!empty(old('product_pack_flag')) && old('product_pack_flag') == 1)
                            <option value="0">{{ 'Product Code' }}</option>
                            <option value="1" selected>{{ 'FOC' }}</option>
                            @else
                            <option value="0">{{ 'Product Code' }}</option>
                            <option value="1">{{ 'FOC' }}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Prodcut Code:</label>
                        <div class="row">
                            @if(!empty(old('generate_product_code_no')) || !empty(old('product_code_no')))
                            <p id="disabled_btn" class="col-3 btn btn-primary disabled">Generate </p>

                            @if(!empty(old('generate_product_code_no')))
                            <input type="hidden" name="generate_product_code_no" id="generated_barcode" value="{{old('generate_product_code_no')}} " required focus>
                            <p id="generated_barcode_p" class="form-control col-9"> {{old('generate_product_code_no')}}</p>
                            @else
                            <input type="number" min=0 name="product_code_no" autocomplete="off" class="form-control col-9" id="barcode" placeholder="Barcode" value="{{old('product_code_no')}}" />
                            @endif
                            @else

                            <p id="pcode_gen" class="col-3 btn btn-primary ">Generate </p>
                            <input type="number" min=0 name="product_code_no" autocomplete="off" class="form-control col-9" id="barcode" placeholder="Barcode">
                            <input type="hidden" name="generate_product_code_no" id="generated_barcode">
                            <p id="generated_barcode_p" class="form-control col-9 d-none"></p>
                            @endif

                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <label class="control-label"> Name <span class="text-danger">*</span>:</label>
                        <input type="text" name="product_name" class="form-control" value="{{ old('product_name') }}" id="product_name" placeholder="Name" required focus autocomplete="off">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> HIP ? Structure <span class="text-danger">*{{old('type')}}</span>:</label>
                        <select class="form-control" name="type" required focus>
                            <option value="">{{ '--HIP ? Structure--' }}</option>
                            @if (!empty(old('type')) && old('type') == 0)
                            <option value="0" selected>{{ 'HIP' }}</option>
                            <option value="1">{{ 'Structure' }}</option>
                            @elseif(!empty(old('type')) && old('type') == 1)
                            <option value="0">{{ 'HIP' }}</option>
                            <option value="1" selected>{{ 'Structure' }}</option>
                            @else
                            <option value="0">{{ 'HIP' }}</option>
                            <option value="1">{{ 'STRUCTURE' }}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label">Grade <span class="text-danger">*</span>:</label>
                        <select class="form-control" name="product_grade_id" required focus>
                            <option value="">{{ '--Local ? Import--' }}</option>
                            @if (!empty(old('type')) && old('type') == 0)
                            <option value="0" selected>{{ 'Local' }}</option>
                            <option value="1">{{ 'Import' }}</option>
                            @elseif(!empty(old('type')) && old('type') == 1)
                            <option value="0">{{ 'Local' }}</option>
                            <option value="1" selected>{{ 'Import' }}</option>
                            @else
                            <option value="0">{{ 'Local' }}</option>
                            <option value="1">{{ 'Import' }}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Category <span class="text-danger">*</span>:</label>
                        <select class="form-control" id='category_id' name="{{'category_id'}}" required focus>
                            <option value="">{{ '--Select One Category--' }}</option>
                            @if($categories != null)
                            @foreach ($categories as $ln)
                            <option value="{{$ln->id}}" {{ old('category_id') == $ln->id ? 'selected': '' }}>{{ $ln->product_category_code ." | ".  $ln->product_category_name }}</option>
                            @endforeach
                            @elseif ($category != null)
                            @foreach ($category as $category_item)
                            <option value="{{$category_item->product_category_id}}" {{ old('category_id') == $category_item->id ? 'selected': '' }}>{{ $category_item->product_category_code ." | ".  $category_item->product_category_name  }}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Group <span class="text-danger">*</span>:</label>
                        <select class="form-control" id='group_id' name="{{'group_id'}}" required focus>
                            <option value="">{{ '--Select One Group--' }}</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Pattern <span class="text-danger">*</span>:</label>
                        <select class="form-control" id='pattern_id' name="{{'pattern_id'}}" required>
                            <option value="">{{ '--Select One Pattern--' }}</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Design <span class="text-danger">*</span>:</label>
                        <select class="form-control" id='design_id' name="{{'design_id'}}" required>
                            <option value="">{{ '--Select One Design--' }}</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Unit <span class="text-danger" required focus>*</span>:</label>
                        <select class="form-control" id="product_unit_name" name="unit_id" required focus>
                            <option value="" disabled selected>-- Please select unit --</option>
                            @foreach($units as $unit)
                            <option value="{{$unit->id}}" {{ old('unit_id') == $unit->id ? 'selected': '' }}>{{ $unit->product_unit_name  ." | ".  $unit->product_unit_code  }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Supplier <span class="text-danger" required focus>*</span>:</label>
                        <select class="form-control" id="vendor_name" name="supplier_id" required focus>
                            <option value="" disabled selected>-- Please select supplier --</option>
                            @foreach($vendors as $vendor)
                            <option value="{{$vendor->id}}" {{ old('supplier_id') == $vendor->id ? 'selected': '' }}>{{ $vendor->vendor_name ." | ". $vendor->vendor_code  }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Brand <span class="text-danger" required focus>*</span>:</label>
                        <select class="form-control" id="product_brand_name" name="brand_id" required focus>
                            <option value="" disabled selected>-- Please select brand --</option>
                            @foreach($brands as $brand)
                            <option value="{{$brand->id}}" {{ old('brand_id') == $brand->id ? 'selected': '' }}>{{ $brand->product_brand_name ." | ".  $brand->product_brand_code  }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <!-- <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Unit <span class="text-danger" required focus>*</span>:</label>
                        <div class="form-group">
                            <input type="hidden" name="unit_id" id="product_unit_id">
                            <input type="text" id="product_unit_name" class="form-control input-lg" autocomplete="off" placeholder="Enter One Unit " required focus />
                            <div id="product_unit_name_list"></div>
                        </div>
                        @csrf
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Supplier <span class="text-danger" required focus>*</span>:</label>
                        <div class="form-group">
                            <input type="hidden" name="supplier_id" id="product_vendor_id">
                            <input type="text" id="product_vendor_name" class="form-control input-lg" autocomplete="off" placeholder="Enter One Supplier " required focus />
                            <div id="product_vendor_name_list"></div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Brand <span class="text-danger" required focus>*</span>:</label>
                        <div class="form-group">
                            <input type="hidden" name="brand_id" id="product_brand_id">
                            <input type="text" id="product_brand_name" class="form-control input-lg" autocomplete="off" placeholder="Enter One Brand" required focus />
                            <div id="product_brand_name_list"></div>
                        </div>
                    </div>
                </div> -->

            </div>
        </form>
    </div>
    <div class="col-md-6 col-sm-12 col-lg-3">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form action="{{ route('doc_store',$doc_id) }}" method="POST" class="mb-3">
            @csrf
            <div class="bg-light p-3">
                <div class="row pb-3">
                    <div class="col text-right">
                        <button type="submit" id="save_doc" class="btn btn-primary text-right {{ $pcount == 0 ? 'd-none': ''}} ">{{ 'Save Doc' }} <i class="fas fa-save"></i> <i class="fas fa-file-word"></i></button>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">Document No :</div>
                    <div class="col">
                        <input type="hidden" name="doc_id" value="{{$doc_id}}" />
                        <input type="text" disabled name="document_no" value="{{$document_no}}" />
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">Requested by :</div>
                    <div class="col">
                        <input type="hidden" name="prepared_by" value="{{ Auth::id() }}" />
                        {{ Auth::user()->name }}
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">Document Prepared Date</div>
                    <div class="col"> {{ now() }}</div>
                </div>
                <input type="hidden" name="pcode_id" id="pcode_ids" />
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <a id="modal_btn" class="btn btn-primary {{ $pcount == 20 ? 'd-none': ''}} ">
                        New Code <i class="fas fa-plus"></i>
                    </a>
                    <p class="text-danger font-weight-bold">{{ $pcount == 20 ? 'One Document Can Save Only 20 Products': ''}} </p>
                </div>
            </div>
        </form>
    </div>
</div>
<table class="table table-striped table-hover table-bordered bg-white {{ $pcount == 0 ? 'd-none': ''}} ">
    <tr>
        <th>No</th>
        <th>Product Type</th>
        <th>Type</th>
        <th>Grade</th>
        <th>Product Code No</th>
        <th>Product Name</th>
        <th>Category</th>
        <th>Group</th>
        <th>Pattern</th>
        <th>Design</th>
        <th>Supplier</th>
        <th>Brand</th>
        <th>Unit</th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($products as $product)
    <tr>
        <td>{{ ++$i}}</td>
        <td>{{ $product->product_pack_flag === 0 ? 'Product Code' : 'FOC' }}</td>
        <td>{{ $product->type === 0 ? 'HIP' : 'Structure' }}</td>
        <td>{{ $product->product_grade_id === 0 ? 'Local' : 'Import' }}</td>
        <td>{{ $product->product_code_no }}</td>
        <td>{{ $product->product_name }}</td>
        <td>{{ $product->category_id ? $product->categories->product_category_name : 'Uncategorized' }}</td>
        <td>{{ $product->group_id ? $product->groups->product_group_name : 'Uncategorized' }}</td>
        <td>{{ $product->pattern_id ? $product->patterns->product_pattern_name : 'Uncategorized' }}</td>
        <td>{{ $product->design_id ? $product->designs->product_design_name : 'Uncategorized' }}</td>
        <td>{{ $product->supplier_id ? $product->suppliers->vendor_name : 'Uncategorized' }}</td>
        <td>{{ $product->brand_id ? $product->brands->product_brand_name : 'Uncategorized' }}</td>
        <td>{{ $product->unit_id ? $product->units->product_unit_name : 'Uncategorized' }}</td>
        <td>
            @php
            $pcode_ids = $pcode_ids .",". $product->id;
            @endphp

            <!-- @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete <i class="fas fa-trash"></i></button> -->
            <button type="button" class="btn btn-info btn-lg btn btn-danger" data-toggle="modal" data-target="#myModal_{{$product->id}}">Delete</button>
            <div id="myModal_{{$product->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <p class="text-danger m-3 text-center" style="font-size : 1.3em;">Are you sure you want to delete this product code.</p>
                        </div>
                        <form action="{{ route('pcode_destory',$product->id) }}" method="POST">
                            @csrf

                            @method('DELETE')
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">Delete Now <i class="fas fa-trash"></i></button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

        </td>
    </tr>
    @endforeach
</table>
<div class="text-md-right text-center mb-3">
    {!! $products->links() !!}
</div>
@endsection

@section('js')
<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }
</style>
<script type="text/javascript">
    $(document).ready(function() {

        $("#pcode_gen").click(function() {
            if ($(this).hasClass('disabled')) {
                return false;
            } else {
                // do whatever when it's active.
                var barcode = $("#barcode").val();
                if (barcode == '') {
                    $.ajax({
                        url: '/pcode_generate/',
                        type: 'get',
                        dataType: 'json',
                        success: function(response) {
                            var len = 0;
                            if (response.data != null) {
                                len = response.data.length;
                            }
                            if (len > 0) {
                                var pcode = response.data;
                                $("#generated_barcode").val(pcode);
                                $("#generated_barcode_p").text(pcode);
                                $("#generated_barcode_p").removeClass('d-none');
                                $("#barcode").addClass('d-none');
                                $("#pcode_gen").addClass('disabled');
                            }
                        }
                    })
                }
            }

        });
        var doc_id = $("#doc_id").val();
        var pcode_ids = '<?php echo $pcode_ids; ?>';
        $("#pcode_ids").val(pcode_ids);
        <?php if ($errors->any()) { ?> $("#pcode").removeClass("d-none");
            $("#pcode").addClass("d-block");
            $("#modal_btn").addClass("d-none");
        <?php } ?> $('#modal_btn').click(function() {
            $("#pcode").removeClass("d-none");
            $("#pcode").addClass("d-block");
            $("#modal_btn").addClass("d-none");
        });
        // product name check form erp
        $('#product_name').focusout(function() {
            var name = $('#product_name').val();
            $.ajax({
               
                url: '/product_Codes/nameCheck',
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    "name" : name },
                dataType: 'json',
                success: function(response) {
                    var len = 0;
                    console.log(response.result);
                    if (response.result == 'already exist') {
                        $('#product_name').removeClass('border-success');
                        $('#product_name').addClass('border-danger'); 
                        alert('Product Name Already Exist!!');
                        $('#product_name').val('');
                    }else{
                        $('#product_name').removeClass('border-danger'); 
                        $('#product_name').addClass('border-success');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            })
        });
        $('#category_id').change(function() {
            var id = $(this).val();
            console.log(id);
            $('#group_id').find('option').not(':first').remove();
            $.ajax({
                url: '/product_Codes/cat/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    console.log(response.data);
                    var len = 0;
                    if (response.data != null) {
                        len = response.data.length;
                    }
                    if (len > 0) {
                        for (var i = 0; i < len; i++) {
                            var id = response.data[i].product_group_id;
                            var name = response.data[i].product_group_name + " | " + response.data[i].product_group_code;
                            // console.log(name);
                            var option = "<option value='" + id + "'>" + name + "</option>";
                            $("#group_id").append(option);
                        }
                    }
                }
            })
        });

        $('#group_id').change(function() {
            var id = $(this).val();
            $('#pattern_id').find('option').not(':first').remove();
            $.ajax({
                url: '/product_Codes/gp/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    var len = 0;
                    if (response.data != null) {
                        len = response.data.length;
                    }
                    if (len > 0) {
                        for (var i = 0; i < len; i++) {
                            var id = response.data[i].product_pattern_id;
                            var name = response.data[i].product_pattern_name + " | " + response.data[i].product_pattern_code;
                            // console.log(name);
                            var option = "<option value='" + id + "'>" + name + "</option>";
                            $("#pattern_id").append(option);
                        }
                    }
                }
            })
        });
        $('#pattern_id').change(function() {
            var id = $(this).val();
            $('#design_id').find('option').not(':first').remove();
            $.ajax({
                url: '/product_Codes/ptn/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    var len = 0;
                    if (response.data != null) {
                        len = response.data.length;
                    }
                    if (len > 0) {
                        for (var i = 0; i < len; i++) {
                            var id = response.data[i].product_design_id;
                            var name = response.data[i].product_design_code;
                            var name = response.data[i].product_design_name + " | " + response.data[i].product_design_code;
                            var option = "<option value='" + id + "'>" + name + "</option>";
                            $("#design_id").append(option);
                        }
                    }
                }
            })
        });
        /////Autocomplete//////
        $('#product_unit_name').keyup(function() {
            var query = $(this).val();
            var _token = $('input[name="_token"]').val();
            if (query != '') {
                $.ajax({
                    url: "{{ route('autocomplete.fetch') }}",
                    method: "POST",
                    data: {
                        query: query,
                        _token: _token
                    },
                    success: function(data) {
                        $('#product_unit_name_list').fadeIn();
                        $('#product_unit_name_list').html(data);
                    }
                });
            }
        });
        $(document).on('click', 'li', function() {
            $('#product_unit_id').val($('#unitID').text());
            $('#product_unit_name').val($('#unitName').text());
            $('#product_unit_name_list').fadeOut();
        });
        $('#product_vendor_name').keyup(function() {
            var query = $(this).val();
            var _token = $('input[name="_token"]').val();
            if (query != '') {
                $.ajax({
                    url: "{{ route('autocomplete.vendorfetch') }}",
                    method: "POST",
                    data: {
                        query: query,
                        _token: _token
                    },
                    success: function(data) {
                        $('#product_vendor_name_list').fadeIn();
                        $('#product_vendor_name_list').html(data);
                    }
                });
            }
        });
        $(document).on('click', 'li', function() {
            $('#product_vendor_id').val($('#vendorID').text());
            $('#product_vendor_name').val($('#vendorName').text());
            $('#product_vendor_name_list').fadeOut();
        });
        $('#product_brand_name').keyup(function() {
            var query = $(this).val();
            if (query != '') {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "{{ route('autocomplete.brandfetch') }}",
                    method: "POST",
                    data: {
                        query: query,
                        _token: _token
                    },
                    success: function(data) {
                        $('#product_brand_name_list').fadeIn();
                        $('#product_brand_name_list').html(data);
                    }
                });
            }
        });
        $(document).on('click', 'li', function() {
            $('#product_brand_id').val($('#brandID').text());
            $('#product_brand_name').val($('#brandName').text());
            $('#product_brand_name_list').fadeOut();
        });
    });
</script>
@endsection