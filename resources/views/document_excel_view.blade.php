@php
$pcount = count($products);
$pcode_ids = "";
if ($document->status == 0) {
$doc_status = 'Requested';
} else if ($document->status == 1) {
$doc_status = 'Approved';
} else {
$doc_status = 'Finished';
}
@endphp
<style>
    td,
    th {
        border: 1px solid #000;
    }
</style>

<table class="table table-striped table-hover table-bordered bg-white ">

    <tr height="30">
        <td width="50" colspan="2"><img src="assets/logo.png" width="80px" hight="auto" sytle="margin:auto" /></td>
        <td colspan="9" style="word-wrap: break-word; text-align:center; margin:auto;">Document No : {{$document->document_no}} ( {{$doc_status}} )</td>
    </tr>

    <tr>
        <td colspan="11"></td>
    </tr>
    <tr style="word-wrap: break-word; text-align:center;">
        <th width="5" style="word-wrap: break-word; text-align:center;">No</th>
        <th width="15" style="word-wrap: break-word; text-align:center;">Type</th>
        <th width="20" style="word-wrap: break-word; text-align:center;">Product Code No</th>
        <th width="40" style="word-wrap: break-word; text-align:center;">Product Name</th>
        <th width="25" style="word-wrap: break-word; text-align:center;">Category</th>
        <th width="20" style="word-wrap: break-word; text-align:center;">Group</th>
        <th width="20" style="word-wrap: break-word; text-align:center;">Pattern</th>
        <th width="20" style="word-wrap: break-word; text-align:center;">Design</th>
        <th width="20" style="word-wrap: break-word; text-align:center;">Supplier</th>
        <th width="20" style="word-wrap: break-word; text-align:center;">Brand</th>
        <th width="10" style="word-wrap: break-word; text-align:center;">Unit</th>
    </tr>

    @foreach ($products as $product)
    <tr>
        <td width="5" style="text-align:center;">{{ ++$i}}</td>
        <td width="15" style="text-align:center;">{{ $product->type === 0 ? 'HIP' : 'Structure' }}</td>
        <td width="20" style="text-align:center;">'{{ $product->product_code_no }}</td>
        <td width="40" style="word-wrap: break-word; text-align:center;">{{ $product->product_name }}</td>
        <td width="25" style="word-wrap: break-word; text-align:center;">{{ $product->category_id ? $product->categories->product_category_name : 'Uncategorized' }}</td>
        <td width="20" style="word-wrap: break-word; text-align:center;">{{ $product->group_id ? $product->groups->product_group_name : 'Uncategorized' }}</td>
        <td width="20" style="word-wrap: break-word; text-align:center;">{{ $product->pattern_id ? $product->patterns->product_pattern_name : 'Uncategorized' }}</td>
        <td width="20" style="word-wrap: break-word; text-align:center;">{{ $product->design_id ? $product->designs->product_design_name : 'Uncategorized' }}</td>
        <td width="20" style="word-wrap: break-word; text-align:center;">{{ $product->supplier_id ? $product->suppliers->vendor_name : 'Uncategorized' }}</td>
        <td width="20" style="word-wrap: break-word; text-align:center;">{{ $product->brand_id ? $product->brands->product_brand_name : 'Uncategorized' }}</td>
        <td width="10" style="word-wrap: break-word; text-align:center;">{{ $product->unit_id ? $product->units->product_unit_name : 'Uncategorized' }}</td>
        @php
        $pcode_ids = $pcode_ids .",". $product->id;
        @endphp
    </tr>
    @endforeach
    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <td colspan="3">Prepare by : {{$document->prepared->name}}</td>
        <td colspan="2"><span style="padding-left:50px;">Checked by : {{ $document->checked_by ? $document->checked->name : '-' }}</span></td>
        <td colspan="3">Approved by : {{ $document->approved_by ? $document->approved->name : '-' }}</td>
        <td colspan="3">ERP inseted by : {{ $document->exported_by ? $document->exported->name : '-' }}</td>
    </tr>
    <tr>
        <td colspan="3">Prepared at : {{ $document->created_at ?  $document->created_at : '-'}} </td>
        <td colspan="2"><span style="padding-left:50px;">Checked at : {{ $document->checked_at ? $document->checked_at : '-' }} </span></td>
        <td colspan="3">Approved at : {{ $document->approved_at ? $document->approved_at : '-' }} </td>
        <td colspan="3">ERP inseted at : {{ $document->exported_at ? $document->exported_at : '-' }} </td>
    </tr>

</table>