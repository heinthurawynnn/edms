@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show product Design<h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('product_Designs.index') }}"> Back</a>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Design Id:</strong>
                {{ $product_Design->product_design_id }}    
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Design Code:</strong>
                {{ $product_Design->product_design_code }}    
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Design Name:</strong>
                {{ $product_Design->product_design_name }}    
            </div>
        </div>
    </div>
@endsection